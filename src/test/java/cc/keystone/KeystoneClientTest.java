package cc.keystone;

import cc.keystone.client.KeystoneAutoConfig;
import cc.keystone.client.KeystoneClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static cc.gateway.Constants.DEFAULT_CLUSTER_NAME;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/8/31.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Import(KeystoneAutoConfig.class)
public class KeystoneClientTest {

    @Inject
    private KeystoneClient keystoneClient;

    @Test
    public void login() {
        keystoneClient.login(DEFAULT_CLUSTER_NAME, "console", "zhtsC1002");
    }
}