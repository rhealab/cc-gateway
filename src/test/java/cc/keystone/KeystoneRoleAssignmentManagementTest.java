package cc.keystone;

import cc.keystone.client.KeystoneAutoConfig;
import cc.keystone.client.KeystoneRoleAssignmentManagement;
import cc.keystone.client.domain.RoleAssignment;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static cc.gateway.Constants.DEFAULT_CLUSTER_NAME;
import static org.junit.Assert.assertEquals;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Import(KeystoneAutoConfig.class)
public class KeystoneRoleAssignmentManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(KeystoneRoleAssignmentManagementTest.class);
    @Inject
    private KeystoneRoleAssignmentManagement keystoneClient;

    @Test
    public void getRoleAssignmentsByUserId() {
        List<RoleAssignment> roleAssignments = keystoneClient.getUserRoleAssignments(DEFAULT_CLUSTER_NAME, "admin");
        logger.debug("size={}", roleAssignments.size());
    }

    @Test
    public void testCache() {
        LoadingCache<String, String> cachedTokens = CacheBuilder.newBuilder()
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(CacheLoader.from(this::loadMockToken));

        assertEquals("abc", cachedTokens.getUnchecked("admin"));
    }

    private String loadMockToken() {
        return "abc";
    }
}