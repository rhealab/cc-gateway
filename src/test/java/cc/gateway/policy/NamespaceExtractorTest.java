package cc.gateway.policy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author wangchunyang@gmail.com
 */
public class NamespaceExtractorTest {
    @Test
    public void extractNamespace() throws Exception {
        assertEquals("ns1", NamespaceExtractor.extract("http://localhost:8888/api/v1/namespaces/ns1"));
        assertEquals("ns1", NamespaceExtractor.extract("http://localhost:8888/api/v1/namespaces/ns1/secrets/s1"));
        assertNull(NamespaceExtractor.extract("http://localhost:8888/api/v1/namespaces"));
        assertNull(NamespaceExtractor.extract("http://localhost:8888/api/v1/sysadmin"));
        assertNull(NamespaceExtractor.extract("http://localhost:8888/api/v1/namespaces_management"));
    }
}