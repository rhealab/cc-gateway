package cc.gateway.policy;

import cc.gateway.policy.data.GatewayRolePolicyRepository;
import org.junit.Test;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.inject.Inject;
import java.util.List;

import static cc.gateway.policy.PolicyRole.*;
import static cc.gateway.policy.domain.HttpMethod.GET;
import static cc.gateway.policy.domain.ServiceType.BACKEND;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

/**
 * @author wangchunyang@gmail.com
 */
public class PolicyManagerTest {
    @Inject
    private GatewayRolePolicyRepository repository;

    @Test
    public void getAllowedRoles() {
        PolicyManager manager = new PolicyManager(repository);
        manager.init();
        List<PolicyRole> roles = manager.getRoles(BACKEND, "http://localhost:8809/gw/be/api/v1/kubernetes/namespaces/ns1", GET);
        assertThat(roles, hasSize(3));
        assertThat(roles, hasItem(NS_ADMIN));
        assertThat(roles, hasItem(DEVELOPER));
        assertThat(roles, hasItem(SYS_ADMIN));


        roles = manager.getRoles(BACKEND, "http://localhost:8809/gw/be/api/v1/kubernetes/namespaces/ns1/storage/s1", GET);
        assertThat(roles, hasSize(2));
        assertThat(roles, hasItem(NS_ADMIN));
        assertThat(roles, hasItem(DEVELOPER));

        roles = manager.getRoles(BACKEND, "http://localhost:8809/gw/be/api/v1/sys/nodes", GET);
        assertThat(roles, hasSize(1));
        assertThat(roles, hasItem(SYS_ADMIN));

        roles = manager.getRoles(BACKEND, "http://localhost:8809/gw/be/api/v1/sysadmin_test/nodes", GET);
        assertThat(roles, hasSize(0));
    }

    @Test
    public void pathMatcher() {
        PathMatcher matcher = new AntPathMatcher();
        assertTrue(matcher.match("/gw/as/api", PolicyHelper.extractPath("http://localhost:8809/gw/as/api")));
        assertFalse(matcher.match("/gw/as/api", PolicyHelper.extractPath("http://localhost:8809/xx/gw/as/api")));
        assertFalse(matcher.match("/gw/as/api", PolicyHelper.extractPath("http://localhost:8809/gw/as/api/v1")));
        assertFalse(matcher.match("/gw/as/api/", PolicyHelper.extractPath("http://localhost:8809/gw/as/api")));
        assertTrue(matcher.match("/gw/as/api/", PolicyHelper.extractPath("http://localhost:8809/gw/as/api/")));
        assertFalse(matcher.match("/gw/as/api", PolicyHelper.extractPath("http://localhost:8809/gw/as/api/")));
        assertFalse(matcher.match("/gw/as/api", ""));


        assertTrue(matcher.match("/gw/be/api/v1/namespaces/*/apps/**",
                PolicyHelper.extractPath("http://localhost:8809/gw/be/api/v1/namespaces/ns1/apps")));

        assertTrue(matcher.match("/gw/be/api/v1/namespaces/*/apps/**",
                PolicyHelper.extractPath("http://localhost:8809/gw/be/api/v1/namespaces/ns1/apps/")));

        assertTrue(matcher.match("/gw/be/api/v1/namespaces/*/apps/**",
                PolicyHelper.extractPath("http://localhost:8809/gw/be/api/v1/namespaces/ns1/apps/app1")));
        assertTrue(matcher.match("/gw/be/api/v1/namespaces/*",
                PolicyHelper.extractPath("http://localhost:8809/gw/be/api/v1/namespaces/ns1")));

        assertTrue(matcher.match("/gw/be/api/v1/namespaces",
                PolicyHelper.extractPath("http://localhost:8809/gw/be/api/v1/namespaces")));
    }
}