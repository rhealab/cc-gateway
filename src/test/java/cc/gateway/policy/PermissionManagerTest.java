package cc.gateway.policy;

import cc.gateway.policy.domain.ServiceType;
import cc.keystone.client.KeystoneRoleAssignmentManagement;
import cc.keystone.client.domain.RoleAssignment;
import cc.keystone.client.domain.RoleType;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import static cc.gateway.Constants.DEFAULT_CLUSTER_NAME;
import static cc.gateway.policy.domain.HttpMethod.GET;
import static cc.gateway.policy.domain.ServiceType.API_SERVER;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author wangchunyang@gmail.com
 */
public class PermissionManagerTest {
    @Test
    public void isAnonymousAllowed() {
        PolicyManager accessControlManager = mock(PolicyManager.class);
        KeystoneRoleAssignmentManagement KeystoneRoleAssignmentManagement = mock(KeystoneRoleAssignmentManagement.class);
        PermissionManager manager = new PermissionManager(accessControlManager, KeystoneRoleAssignmentManagement);

        when(accessControlManager.getRoles(ServiceType.API_SERVER, "http://localhost:8809/gw/as/api", GET)).thenReturn(ImmutableList.of(PolicyRole.ANONYMOUS));

        assertTrue(manager.isAnonymousAllowed(API_SERVER, "http://localhost:8809/gw/as/api", GET));
    }

    @Test
    public void checkPermission() {
        KeystoneRoleAssignmentManagement assignmentManagement = mock(KeystoneRoleAssignmentManagement.class);
        PolicyManager accessControlManager = mock(PolicyManager.class);
        KeystoneRoleAssignmentManagement KeystoneRoleAssignmentManagement = mock(KeystoneRoleAssignmentManagement.class);
        PermissionManager permissionManager = new PermissionManager(accessControlManager, KeystoneRoleAssignmentManagement);

        RoleAssignment sysAdmin = new RoleAssignment("user1", "user1", RoleType.SYS_ADMIN, null);
        RoleAssignment nsAdmin = new RoleAssignment("user2", "user2", RoleType.NS_ADMIN, "ns1");
        RoleAssignment dev = new RoleAssignment("user2", "user2", RoleType.DEVELOPER, "ns1");
        when(assignmentManagement.getUserRoleAssignments(DEFAULT_CLUSTER_NAME, "user1")).thenReturn(ImmutableList.of(sysAdmin));
        when(assignmentManagement.getUserRoleAssignments(DEFAULT_CLUSTER_NAME, "user2")).thenReturn(ImmutableList.of(nsAdmin, dev));

        when(accessControlManager.getRoles(ServiceType.API_SERVER, "http://localhost:8888/api/v1/sysadmin", GET)).thenReturn(ImmutableList.of(PolicyRole.SYS_ADMIN));

        assertTrue(permissionManager.isUserAllowed(ServiceType.API_SERVER, "http://localhost:8888/api/v1/sysadmin", GET, "user1"));
        assertFalse(permissionManager.isUserAllowed(ServiceType.API_SERVER, "http://localhost:8888/api/v1/sysadmin", GET, "user2"));

        when(accessControlManager.getRoles(ServiceType.API_SERVER, "http://localhost:8888/api/v1/namespaces/ns1/secretes/s1", GET))
                .thenReturn(ImmutableList.of(PolicyRole.NS_ADMIN, PolicyRole.DEVELOPER));
        assertTrue(permissionManager.isUserAllowed(ServiceType.API_SERVER, "http://localhost:8888/api/v1/namespaces/ns1/secretes/s1", GET, "user2"));
        assertFalse(permissionManager.isUserAllowed(ServiceType.API_SERVER, "http://localhost:8888/api/v1/namespaces/ns1/secretes/s1", GET, "user1"));
    }
}