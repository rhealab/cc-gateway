package cc.gateway.security;

import cc.gateway.Constants;
import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.policy.domain.ServiceType;
import cc.gateway.security.data.PrivilegeGroup;
import cc.gateway.security.data.SvcPrivilege;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by mye(Xizhe Ye) on 17-11-7.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SecurityMgrTest {
  @Inject
  SecurityMgr securityMgr;


  @Test
  public void testNewSecurityGroup() {
    securityMgr.addPrivilegeGroup("SYS_ADMIN");
    List<String> groups = securityMgr.getPrivilegeGroups();
    Assert.assertTrue(groups.size() > 0);
  }

  @Test
  public void testSaveSecurityGroup() {
    String groupname = "SYS_ADMIN";
    PrivilegeGroup group = new PrivilegeGroup();
    group.setGroupName(groupname);
    group.setId(123);
    group.setUserId("2312");
    securityMgr.addUser2PrivilegeGroup("2312", groupname, true);

    List<PrivilegeGroup> group1 = securityMgr.getUserIdsByGroup(groupname);
    Assert.assertTrue(group1.size() > 0);
  }

  @Test
  public void testSaveSvcPrivilege() {
    String serviceType = "API_SERVER";
    securityMgr.addSvcPrivilege2List(serviceType);
    List<String> lists = securityMgr.getSvcPrivilegeList();
    Assert.assertTrue(lists.size() > 0);
  }

  @Test
  public void testStorePrivilege() {
    String serviceType = "API_SERVER";
    String path = "/gw/as/api/v1/endpoints";
    SvcPrivilege svcPrivilege = new SvcPrivilege();
    svcPrivilege.setMethodType(HttpMethod.POST);
    svcPrivilege.setServiceName(serviceType);
    svcPrivilege.setUserGroup(Constants.GROUP_ANONYMOUS);
    svcPrivilege.setUrl(path);
    securityMgr.storeSvcPrivilege(serviceType, svcPrivilege, true);
    List<SvcPrivilege> lists = securityMgr.getSvcPrivileges(serviceType);
    Assert.assertTrue(lists.size() > 0);
  }

  @Test
  public void testAddNewSvcPrivilege() {
    securityMgr.addSvcPrivilege2List("API_SERVER");
    List<String> lists = securityMgr.getSvcPrivilegeList();
    Assert.assertTrue(lists.contains("SYS_ADMIN"));
  }


  @Test
  public void testCheckUserValid() {
    String serviceType = "API_SERVER";
    String path = "http://java.sun.com/gw/as/api/v1/endpoints";
    HttpMethod method = HttpMethod.POST;
    String userId = "2312";
    boolean flag = securityMgr.checkSvcAllowByUserId(userId, serviceType, path, method);
    Assert.assertTrue(flag);
  }


  @Test
  public void testisAnonymousAllowd() {
    String serviceType = "API_SERVER";
    String path = "http://java.sun.com/gw/as/api/v1/endpoints";
    HttpMethod method = HttpMethod.POST;
    String userId = "123";

    boolean flag = securityMgr.isAnonymousAllowed(serviceType, path, method);
    Assert.assertTrue(flag);

  }
}
