package cc.gateway.security;

import cc.gateway.token.SecurityToken;
import cc.gateway.token.Token;
import cc.gateway.token.TokenManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mye(Xizhe Ye) on 17-12-27.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenManagerTest {
  @Inject
  TokenManager tokenManager;

  @Test
  public void testToken() {
    String userId = "mye";
    String receiver = "DF_CORE_SVC";
    SecurityToken token = new SecurityToken();
    token.setSource("GW");
    token.setUserId(userId);
    token.setReceiver(receiver);
    Date dt = new Date();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(dt);
    calendar.add(Calendar.DATE, 30);
    token.setExpirationTime(dt);
    String rawToken = tokenManager.encodeSecurityToken(token);
    Assert.assertTrue(rawToken.length() > 0);
  }

  @Test
  public void testdecode() {
      String toekn = "eyJhbGciOiJSUzI1NiJ9.eyJ1aWQiOiJmcmFua2llIiwicmYiOmZhbHNlLCJ1Ijoi5p2o5Lyf5Y2OIiwiaXNzIjoiY2MiLCJleHAiOjE1MjY1NTEyOTAsInd0ayI6IjQzYTIyMDFlZjg1NDRlOGViMTU2YjcyYjAzMmVkOWM5In0.qRCfB9nXUCSOpOQVZ9aSSgoJwplnH9mpYtJsz_ESTy6DdOzMaTRxPUKLGWPSrv5wfDTzJD7D3B4sAiHHjG-UKX413h0NE07qL6u-KE5k5IVyxpORGlR0ewxEubHdyV_6TkoySDc7uq_LrVcWWU9YdkqhjaJ88tWJLwnSDhXyn3wS5R1kZwBCKnCjK3yJeXerkJFh5dMSp2WuV4BtzFMdCFGytWgF53zyZ0U86dDcnTcshg-Bydlhn0gcLKKEvQPIcBp1URP5mqMacGfvqZSk1YclwMaxXenl-Mo7pdQy64PRLzhgkNwumS3G-xoftegfKtIrnzfoL3buUXZOSly0ww";
      Token token = tokenManager.decodeToken(toekn);
      Assert.assertTrue(token.isValid());
  }
}
