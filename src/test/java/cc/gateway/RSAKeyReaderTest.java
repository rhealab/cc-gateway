package cc.gateway;

import cc.gateway.token.RSAKeyPairReader;
import cc.gateway.token.SecurityToken;
import cc.gateway.token.TokenManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mye(Xizhe Ye) on 17-11-20.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RSAKeyReaderTest {
  @Inject
  RSAKeyPairReader reader;

  @Inject
  TokenManager tokenManager;

  @Test
  public void testReadPublicKey() {
    RSAPublicKey pubkey = reader.readPublicKey("keys/public.der");
    Assert.assertNotNull(pubkey);

    RSAPrivateKey prikey = reader.readPrivateKey("keys/private.der");
    Assert.assertNotNull(prikey);

  }

  @Test
  public void testEncodeJWT() {
    SecurityToken token = new SecurityToken();
    token.setUserId("23");
    token.setSource("gateway");
    token.setExpirationTime( Calendar.getInstance().getTime());
    String jwt = tokenManager.encodeSecurityToken(token);
    Assert.assertNotNull(jwt);
  }


  @Test
  public void testUserToken() {
    SecurityToken token = new SecurityToken();
    token.setSource("GW");
    token.setUserId("mye");
    Date dt = new Date();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(dt);
    calendar.add(Calendar.DATE, 30);
    token.setExpirationTime(dt);

    String jwt = tokenManager.encodeSecurityToken(token);
    SecurityToken token1 = tokenManager.decodeUserToken(jwt);
    token1.setReceiver("re");
    String jwt2 = tokenManager.encodeSecurityToken(token1);
    SecurityToken token2 = tokenManager.decodeUserToken(jwt2);
    Assert.assertNotNull(token2);
  }

}
