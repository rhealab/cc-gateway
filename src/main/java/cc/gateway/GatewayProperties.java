package cc.gateway;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wangchunyang@gmail.com
 */
@Component
@ConfigurationProperties(prefix = "gateway")
public class GatewayProperties {
    private boolean authEnabled;
    private boolean auditEnabled;
    private String apiServerAdminUsername;
    private String apiServerAdminPassword;

    public boolean isAuthEnabled() {
        return authEnabled;
    }

    public void setAuthEnabled(boolean authEnabled) {
        this.authEnabled = authEnabled;
    }

    public boolean isAuditEnabled() {
        return auditEnabled;
    }

    public void setAuditEnabled(boolean auditEnabled) {
        this.auditEnabled = auditEnabled;
    }

    public String getApiServerAdminUsername() {
        return apiServerAdminUsername;
    }

    public void setApiServerAdminUsername(String apiServerAdminUsername) {
        this.apiServerAdminUsername = apiServerAdminUsername;
    }

    public String getApiServerAdminPassword() {
        return apiServerAdminPassword;
    }

    public void setApiServerAdminPassword(String apiServerAdminPassword) {
        this.apiServerAdminPassword = apiServerAdminPassword;
    }
}
