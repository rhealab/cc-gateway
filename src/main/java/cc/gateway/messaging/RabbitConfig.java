package cc.gateway.messaging;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangchunyang@gmail.com
 */
@Configuration
public class RabbitConfig {
    public static final String REQUEST_AUDIT_Q = "request_audit_q";

    @Bean
    public Queue getGatewayAuditQueue() {
        return new Queue(REQUEST_AUDIT_Q, true);
    }

    @Bean
    public MessageConverter getMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
