package cc.gateway;

import cc.gateway.filters.post.RequestAuditFilter;
import cc.gateway.filters.pre.*;
import cc.gateway.zuul.EnableEnnZuulProxy;
import cc.keystone.client.KeystoneAutoConfig;
import com.google.common.collect.ImmutableList;
import com.netflix.zuul.constants.ZuulConstants;
import org.apache.catalina.connector.Connector;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Optional;


/**
 * @author wangchunyang@gmail.com
 */
@SpringBootApplication
@EnableEnnZuulProxy
@Import(KeystoneAutoConfig.class)
public class GatewayApplication {

    public static void main(String[] args) {
        String connectTimeout = Optional.ofNullable(System.getenv("ZUUL_CONNECT_TIMEOUT"))
                .orElseGet(() -> System.getenv("zuul_connect_timeout"));
        String socketTimeout = Optional.ofNullable(System.getenv("ZUUL_SOCKET_TIMEOUT"))
                .orElseGet(() -> System.getenv("zuul_socket_timeout"));

        System.setProperty(ZuulConstants.ZUUL_DEBUG_REQUEST, "false");
        System.setProperty(ZuulConstants.ZUUL_HOST_CONNECT_TIMEOUT_MILLIS, Optional.ofNullable(connectTimeout).orElse("10000"));
        System.setProperty(ZuulConstants.ZUUL_HOST_SOCKET_TIMEOUT_MILLIS, Optional.ofNullable(socketTimeout).orElse("300000"));

        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public ApiServerPreFilter getApiServerPreFilter() {
        return new ApiServerPreFilter();
    }

    @Bean
    public BackendPreFilter getBackendPreFilter() {
        return new BackendPreFilter();
    }

    @Bean
    public OriginKubernetesPreFilter getOriginKubernetesFilter() {
        return new OriginKubernetesPreFilter();
    }

    @Bean
    public NotificationPreFilter getNotificationPreFiler() {
        return new NotificationPreFilter();
    }

    @Bean
    public RequestAuditFilter getRequestAuditFilter() {
        return new RequestAuditFilter();
    }

    @Bean
    public HarborPreFilter getHarborPreFilter() {
        return new HarborPreFilter();
    }

    @Bean
    public InitializerPreFilter getInitializerPreFilter() {
        return new InitializerPreFilter();
    }

    @Bean
    public AccountPreFilter getAccountPreFilter() {
        return new AccountPreFilter();
    }

    @Bean
    public AuditPreFilter getAuditPreFilter() {
        return new AuditPreFilter();
    }

    @Bean
    public LogPreFilter getLogPreFilter() {
        return new LogPreFilter();
    }

    @Bean
    public MetricPreFilter getMetricPreFilter() {
        return new MetricPreFilter();
    }

    @Bean
    public TensorflowPreFilter getTensorflowPreFilter() {
        return new TensorflowPreFilter();
    }

    @Bean
    public TemplatePreFilter getTemplatePreFilter() {
        return new TemplatePreFilter();
    }

    @Bean
    public WatchPreFilter getWatchPreFilter() {
        return new WatchPreFilter();
    }

    @Bean
    public InternalPreFilter getInternalPreFilter() {
        return new InternalPreFilter();
    }

    @Bean
    public StoragePreFilter getStoragePreFilter() {
        return new StoragePreFilter();
    }

    @Bean
    public DfCorePreFilter getDfCorePreFilter() {
        return new DfCorePreFilter();
    }

    @Bean
    public DfMetricPreFilter getDfMetricPreFilter() {
        return new DfMetricPreFilter();
    }

    @Bean
    public GcPreFilter getGcPreFilter() {
        return new GcPreFilter();
    }

    @Bean
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(ImmutableList.of("*"));
        config.setAllowedHeaders(ImmutableList.of("*"));
        config.setAllowedMethods(ImmutableList.of("*"));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
        factory.addAdditionalTomcatConnectors(createConnector());
        return factory;
    }

    private Connector createConnector() {
        Connector connector = new Connector(TomcatEmbeddedServletContainerFactory.DEFAULT_PROTOCOL);
        connector.setPort(8810);
        return connector;
    }
}
