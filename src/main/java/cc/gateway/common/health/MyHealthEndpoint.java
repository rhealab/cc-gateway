package cc.gateway.common.health;


import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("health")
public class MyHealthEndpoint {

    @GET
    public Health health() {
        Health health = new Health();
        health.setName("cc-gateway");
        health.setVersion("1.0.0");
        health.setStatus("OK");
        health.setTimestamp(new Date());
        return health;
    }
}