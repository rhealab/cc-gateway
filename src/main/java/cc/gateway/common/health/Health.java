package cc.gateway.common.health;

import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
public class Health {
    private String version;
    private Date timestamp;
    private String status;
    private String name;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Health{" +
                ", version='" + version + '\'' +
                ", timestamp=" + timestamp +
                ", status='" + status + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
