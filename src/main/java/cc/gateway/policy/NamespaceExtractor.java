package cc.gateway.policy;

import org.apache.commons.lang3.StringUtils;

/**
 * @author wangchunyang@gmail.com
 */
public class NamespaceExtractor {
    public static String extract(String requestURL) {
        String ns = StringUtils.substringBetween(requestURL, "namespaces/", "/");
        if (StringUtils.isBlank(ns)) {
            ns = StringUtils.substringAfter(requestURL, "namespaces/");
        }

        if (StringUtils.isBlank(ns)) {
            ns = StringUtils.substringBetween(requestURL, "namespace_stats/", "/");
        }

        if (StringUtils.isBlank(ns)) {
            ns = StringUtils.substringAfter(requestURL, "namespace_stats/");
        }

        return StringUtils.isBlank(ns) ? null : ns;
    }
}
