package cc.gateway.policy.data;

import cc.gateway.policy.domain.ServiceType;

import javax.persistence.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2018/3/16.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"serviceType", "path", "httpMethods"}))
public class GatewayRolePolicy {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ServiceType serviceType;

    @Column(nullable = false)
    private String path;

    @Column(nullable = false)
    private String httpMethods;

    @Column(nullable = false)
    private String roles;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getHttpMethods() {
        return httpMethods;
    }

    public void setHttpMethods(String httpMethods) {
        this.httpMethods = httpMethods;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
