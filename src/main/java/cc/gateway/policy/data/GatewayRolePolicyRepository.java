package cc.gateway.policy.data;

import cc.gateway.policy.domain.ServiceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2018/3/16.
 */
public interface GatewayRolePolicyRepository extends JpaRepository<GatewayRolePolicy, Long> {
    List<GatewayRolePolicy> findByServiceType(ServiceType serviceType);

    @Query(value = "SELECT * FROM gateway_role_policy LIMIT 1, 1", nativeQuery = true)
    GatewayRolePolicy findOne();
}