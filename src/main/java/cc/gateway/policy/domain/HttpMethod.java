package cc.gateway.policy.domain;

/**
 * @author wangchunyang@gmail.com
 */
public enum HttpMethod {
    GET, PUT, POST, DELETE, HEAD, OPTIONS, PATCH
}
