package cc.gateway.policy.domain;

/**
 * @author wangchunyang@gmail.com
 */
public enum ServiceType {
    API_SERVER,
    BACKEND,
    ORIGIN,
    NOTIFICATION,
    HARBOR,
    INITIALIZER,
    ACCOUNT,
    AUDIT,
    LOG,
    METRIC,
    WATCH,
    TEMPLATE,
    STORAGE,
    DF_CORE_SVC,
    DF_METRIC_SVC,
    GC,
    TENSORFLOW,
    DF_MONITOR_SVC,
    DEPENDENCY_CHECKER
}
