package cc.gateway.policy.domain;

import cc.gateway.policy.PolicyRole;
import cc.gateway.policy.data.GatewayRolePolicy;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author wangchunyang@gmail.com
 */
public class Policy {
    private long id;
    private ServiceType serviceType;
    @NotNull
    private String path;
    @NotEmpty
    private List<HttpMethod> httpMethods = new ArrayList<>();
    @NotEmpty
    private List<PolicyRole> roles = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<HttpMethod> getHttpMethods() {
        return httpMethods;
    }

    public void setHttpMethods(List<HttpMethod> httpMethods) {
        this.httpMethods = httpMethods;
    }

    public List<PolicyRole> getRoles() {
        return roles;
    }

    public void setRoles(List<PolicyRole> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Policy{" +
                "id=" + id +
                ", serviceType=" + serviceType +
                ", path='" + path + '\'' +
                ", httpMethods=" + httpMethods +
                ", roles=" + roles +
                '}';
    }

    public void addMethod(HttpMethod httpMethod) {
        this.httpMethods.add(httpMethod);
    }

    public void addRole(String role) {
        this.roles.add(PolicyRole.valueOf(role));
    }

    public static List<GatewayRolePolicy> to(List<Policy> policies) {
        return Optional.ofNullable(policies)
                .orElseGet(ArrayList::new)
                .stream()
                .map(Policy::to)
                .collect(Collectors.toList());
    }

    public static GatewayRolePolicy to(Policy policy) {
        GatewayRolePolicy rolePolicy = new GatewayRolePolicy();
        rolePolicy.setId(policy.getId());
        rolePolicy.setServiceType(policy.getServiceType());
        rolePolicy.setPath(policy.getPath());

        String httpMethods = policy.getHttpMethods()
                .stream()
                .map(Enum::name)
                .collect(Collectors.joining(","));
        rolePolicy.setHttpMethods(httpMethods);

        String roles = policy.getRoles()
                .stream()
                .map(Enum::name)
                .collect(Collectors.joining(","));
        rolePolicy.setRoles(roles);

        return rolePolicy;
    }

    public static Policy from(GatewayRolePolicy rolePolicy) {
        Policy policy = new Policy();
        policy.setId(rolePolicy.getId());
        policy.setServiceType(rolePolicy.getServiceType());
        policy.setPath(rolePolicy.getPath());

        String[] httpMethodsStr = rolePolicy.getHttpMethods().split(",");
        List<HttpMethod> httpMethods = Arrays.stream(httpMethodsStr)
                .map(HttpMethod::valueOf)
                .collect(Collectors.toList());
        policy.setHttpMethods(httpMethods);

        String[] roles = rolePolicy.getRoles().split(",");
        policy.setRoles(Arrays.stream(roles).map(PolicyRole::valueOf).collect(Collectors.toList()));
        return policy;
    }
}
