package cc.gateway.policy;

import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.policy.domain.ServiceType;
import cc.keystone.client.KeystoneRoleAssignmentManagement;
import cc.keystone.client.domain.RoleAssignment;
import cc.keystone.client.domain.RoleType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static cc.gateway.Constants.DEFAULT_CLUSTER_NAME;


/**
 * @author wangchunyang@gmail.com
 */
@Component
public class PermissionManager {
    private static final Logger logger = LoggerFactory.getLogger(PermissionManager.class);

    private final PolicyManager policyManager;

    private final KeystoneRoleAssignmentManagement roleAssignmentManagement;

    @Inject
    public PermissionManager(PolicyManager policyManager,
                             KeystoneRoleAssignmentManagement roleAssignmentManagement) {
        this.policyManager = policyManager;
        this.roleAssignmentManagement = roleAssignmentManagement;
    }

    public boolean isAnonymousAllowed(ServiceType serviceType, String url, HttpMethod httpMethod) {
        List<PolicyRole> roles = policyManager.getRoles(serviceType, url, httpMethod);
        boolean result = roles.contains(PolicyRole.ANONYMOUS);
        logger.debug("Allow anonymous access for {} -> {}", url, result);
        return result;
    }

    public boolean isForbidden(ServiceType serviceType, String url, HttpMethod httpMethod) {
        List<PolicyRole> roles = policyManager.getRoles(serviceType, url, httpMethod);
        boolean result = roles.contains(PolicyRole.FORBIDDEN);
        logger.debug("Forbidden access for {} -> {}", url, result);
        return result;
    }

    public boolean isUserAllowed(ServiceType serviceType, String requestUrl, HttpMethod httpMethod, String userId) {
        List<PolicyRole> roles = policyManager.getRoles(serviceType, requestUrl, httpMethod);
        if (roles.size() == 0) {
            logger.debug("The allowed roles are not found for url {}. The access will be denied.", requestUrl);
            return false;
        }

        List<RoleAssignment> roleAssignments = roleAssignmentManagement.getUserRoleAssignments(DEFAULT_CLUSTER_NAME, userId);
        for (PolicyRole role : roles) {
            if (matchRole(NamespaceExtractor.extract(requestUrl), role, roleAssignments)) {
                return true;
            }
        }
        return false;
    }

    private boolean matchRole(String ns, PolicyRole allowedRole, List<RoleAssignment> roleAssignments) {
        if (allowedRole == PolicyRole.SYS_ADMIN) {
            return hasRole(roleAssignments, RoleType.SYS_ADMIN);
        }

        // check NS_ADMIN or DEVELOPER
        if (ns == null) {
            logger.debug("Namespace is not found in url path.");
            return hasRole(roleAssignments, allowedRole.getKeystoneRoleType());
        }

        return hasRoleOnNamespace(roleAssignments, ns, allowedRole.getKeystoneRoleType());

    }

    private boolean hasRoleOnNamespace(List<RoleAssignment> roleAssignments, String ns, RoleType allowedRole) {
        for (RoleAssignment assignment : roleAssignments) {
            RoleType role = assignment.getRole();
            if (ns.equals(assignment.getProject()) && role == allowedRole) {
                return true;
            }
        }
        return false;
    }

    private boolean hasRole(List<RoleAssignment> roleAssignments, RoleType roleType) {
        for (RoleAssignment assignment : roleAssignments) {
            if (assignment.getRole() == roleType) {
                return true;
            }
        }
        return false;
    }
}
