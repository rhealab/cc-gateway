package cc.gateway.policy;

import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.policy.domain.Policy;
import cc.gateway.policy.domain.ServiceType;
import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/3/16.
 */
public class PolicyHelper {

    private static final Logger logger = LoggerFactory.getLogger(PolicyHelper.class);

    public static String extractPath(String requestUrl) {
        try {
            URL url = new URL(requestUrl);
            return url.getPath();
        } catch (MalformedURLException e) {
            return "";
        }
    }

    public static Map<ServiceType, Map<HttpMethod, List<Policy>>> composeServicePoliciesMap(Map<ServiceType, List<Policy>> servicePolicies) {
        return servicePolicies.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> composePoliciesMap(entry.getValue())));
    }

    private static Map<HttpMethod, List<Policy>> composePoliciesMap(List<Policy> policies) {
        Map<HttpMethod, List<Policy>> policiesMap = new HashMap<>();

        for (Policy policy : policies) {
            List<HttpMethod> methods = policy.getHttpMethods();
            for (HttpMethod method : methods) {
                List<Policy> list = policiesMap.computeIfAbsent(method, k -> new ArrayList<>());
                list.add(policy);
            }
        }

        return policiesMap;
    }

    public static List<Policy> loadFromResourceName(ServiceType serviceType, String resourceName) {
        List<Policy> policies = new ArrayList<>();
        Gson gson = new Gson();
        try {

            InputStreamReader reader = new InputStreamReader(Resources.getResource(resourceName).openStream());
            JsonArray jsonArray = gson.fromJson(reader, JsonArray.class);
            jsonArray.forEach(element -> {
                Policy policy = buildPolicy(element);
                policy.setServiceType(serviceType);
                policies.add(policy);
            });

        } catch (Throwable t) {
            logger.error("Failed to parse policy file - {}", resourceName, t);
        }
        return policies;
    }

    private static Policy buildPolicy(JsonElement element) {
        Policy policy = new Policy();
        String[] items = element.getAsString().replaceAll("\\s", "").split(":");
        policy.setPath(items[0]);
        Arrays.stream(items[1].split(",")).forEach(method -> {
                    if (method.equals("*")) {
                        policy.setHttpMethods(Arrays.asList(HttpMethod.values()));
                    } else {
                        policy.addMethod(HttpMethod.valueOf(method));
                    }
                }
        );
        Arrays.stream(items[2].split(",")).forEach(policy::addRole);
        return policy;
    }
}
