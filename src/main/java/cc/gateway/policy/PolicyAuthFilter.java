package cc.gateway.policy;

import cc.gateway.GatewayProperties;
import cc.gateway.error.Err;
import cc.gateway.error.ErrHelper;
import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.token.Token;
import cc.gateway.token.TokenHelper;
import cc.gateway.token.TokenManager;
import cc.keystone.client.KeystoneRoleAssignmentManagement;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import static cc.gateway.Constants.DEFAULT_CLUSTER_NAME;

/**
 * The class should be annotated with @Priority(Priorities.AUTHENTICATION) so it is performed before other
 * filters, if any. For example, this filter must be performed before RolesAllowedRequestFilter when
 * RolesAllowedDynamicFeature is registered. Otherwise the SecurityContext doesn't include roles information.
 * <p>
 * !!! this filter only for gateway policy endpoint, and only sys admin can access the policy management!!!
 *
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/3/20.
 */

@Priority(Priorities.AUTHENTICATION)
@Provider
@Named
public class PolicyAuthFilter implements ContainerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(PolicyAuthFilter.class);

    private final TokenManager tokenManager;

    private final GatewayProperties properties;

    private final KeystoneRoleAssignmentManagement roleAssignmentManagement;

    @Inject
    public PolicyAuthFilter(TokenManager tokenManager,
                            GatewayProperties properties,
                            KeystoneRoleAssignmentManagement roleAssignmentManagement) {
        this.tokenManager = tokenManager;
        this.properties = properties;
        this.roleAssignmentManagement = roleAssignmentManagement;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        if (!properties.isAuthEnabled()) {
            return;
        }

        String method = requestContext.getMethod();
        String path = requestContext.getUriInfo().getPath();
        if (HttpMethod.OPTIONS.name().equals(method)) {
            return;
        }

        if ("health".equals(path)) {
            return;
        }

        String token = TokenHelper.extractToken(requestContext);

        if (Strings.isNullOrEmpty(token)) {
            logger.info("User is not authenticated: token is not found.");
            requestContext.abortWith(ErrHelper.composeResponse(Err.USER_NOT_AUTHENTICATED));
        } else {
            Token decodedToken = tokenManager.decodeToken(token);
            if (!decodedToken.isValid()) {
                logger.info("User is not authenticated: token is not valid. ({}, {})", path, method);
                requestContext.abortWith(ErrHelper.composeResponse(Err.USER_NOT_AUTHENTICATED));
            } else if (!checkPermission(decodedToken)) {
                requestContext.abortWith(ErrHelper.composeResponse(Err.USER_NOT_AUTHORIZED));
            }
        }
    }

    private boolean checkPermission(Token token) {
        return roleAssignmentManagement.isSysAdmin(DEFAULT_CLUSTER_NAME, token.getUserId());
    }
}
