package cc.gateway.policy;

import cc.gateway.policy.data.GatewayRolePolicy;
import cc.gateway.policy.data.GatewayRolePolicyRepository;
import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.policy.domain.Policy;
import cc.gateway.policy.domain.ServiceType;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author wangchunyang@gmail.com
 */
@Component
public class PolicyManager {
    private static final Logger logger = LoggerFactory.getLogger(PolicyManager.class);

    private PathMatcher pathMatcher = new AntPathMatcher();

    private Map<ServiceType, List<Policy>> servicePolicies = new HashMap<>();

    private Map<ServiceType, Map<HttpMethod, List<Policy>>> servicePoliciesMap = new HashMap<>();

    private LoadingCache<ComposedKey, List<PolicyRole>> cachedRoles = CacheBuilder.newBuilder()
            .maximumSize(300)
            .expireAfterAccess(1, TimeUnit.DAYS)
            .build(new CacheLoader<ComposedKey, List<PolicyRole>>() {
                @Override
                public List<PolicyRole> load(@Nonnull ComposedKey key) {
                    return loadRoles(key.serviceType, key.url, key.httpMethod);
                }
            });

    private final GatewayRolePolicyRepository repository;

    @Inject
    public PolicyManager(GatewayRolePolicyRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    public void init() {
        if (repository.findOne() == null) {
            initDb();
        }
        Map<ServiceType, List<Policy>> dbPoliciesMap = getDbPoliciesMap();

        servicePolicies.putAll(dbPoliciesMap);
        servicePoliciesMap.putAll(PolicyHelper.composeServicePoliciesMap(servicePolicies));
    }

    private void initDb() {
        List<GatewayRolePolicy> rolePolicies = getLocalPoliciesMap()
                .entrySet()
                .stream()
                .map(entry -> Policy.to(entry.getValue()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        repository.save(rolePolicies);
    }

    public void refreshServicePolicies() {
        Map<ServiceType, List<Policy>> dbPoliciesMap = getDbPoliciesMap();
        servicePolicies.clear();
        servicePolicies.putAll(dbPoliciesMap);
        logger.info("all policies refreshed!");
    }

    public Map<ServiceType, List<Policy>> getServicePolicies() {
        return servicePolicies;
    }

    private Map<ServiceType, List<Policy>> getLocalPoliciesMap() {
        Map<ServiceType, List<Policy>> localPolicies = new HashMap<>(16);

        localPolicies.put(ServiceType.ACCOUNT, PolicyHelper.loadFromResourceName(ServiceType.ACCOUNT, "policy/account.json"));
        localPolicies.put(ServiceType.API_SERVER, PolicyHelper.loadFromResourceName(ServiceType.API_SERVER, "policy/apiserver.json"));
        localPolicies.put(ServiceType.AUDIT, PolicyHelper.loadFromResourceName(ServiceType.AUDIT, "policy/audit.json"));
        localPolicies.put(ServiceType.BACKEND, PolicyHelper.loadFromResourceName(ServiceType.BACKEND, "policy/backend.json"));
        localPolicies.put(ServiceType.DF_CORE_SVC, PolicyHelper.loadFromResourceName(ServiceType.DF_CORE_SVC, "policy/df_core_svc.json"));
        localPolicies.put(ServiceType.DF_METRIC_SVC, PolicyHelper.loadFromResourceName(ServiceType.DF_METRIC_SVC, "policy/df_metric_svc.json"));
        localPolicies.put(ServiceType.GC, PolicyHelper.loadFromResourceName(ServiceType.GC, "policy/gc.json"));
        localPolicies.put(ServiceType.HARBOR, PolicyHelper.loadFromResourceName(ServiceType.HARBOR, "policy/harbor.json"));
        localPolicies.put(ServiceType.INITIALIZER, PolicyHelper.loadFromResourceName(ServiceType.INITIALIZER, "policy/initializer.json"));
        localPolicies.put(ServiceType.LOG, PolicyHelper.loadFromResourceName(ServiceType.LOG, "policy/log.json"));
        localPolicies.put(ServiceType.METRIC, PolicyHelper.loadFromResourceName(ServiceType.METRIC, "policy/metric.json"));
        localPolicies.put(ServiceType.NOTIFICATION, PolicyHelper.loadFromResourceName(ServiceType.NOTIFICATION, "policy/notification.json"));
        localPolicies.put(ServiceType.ORIGIN, PolicyHelper.loadFromResourceName(ServiceType.ORIGIN, "policy/origin.json"));
        localPolicies.put(ServiceType.STORAGE, PolicyHelper.loadFromResourceName(ServiceType.STORAGE, "policy/storage.json"));
        localPolicies.put(ServiceType.TENSORFLOW, PolicyHelper.loadFromResourceName(ServiceType.TENSORFLOW, "policy/tensorflow.json"));
        localPolicies.put(ServiceType.WATCH, PolicyHelper.loadFromResourceName(ServiceType.WATCH, "policy/watch.json"));
        localPolicies.put(ServiceType.TEMPLATE, PolicyHelper.loadFromResourceName(ServiceType.TEMPLATE, "policy/template.json"));

        return localPolicies;
    }

    private Map<ServiceType, List<Policy>> getDbPoliciesMap() {
        return repository.findAll()
                .stream()
                .collect(Collectors.groupingBy(GatewayRolePolicy::getServiceType,
                        Collectors.mapping(Policy::from, Collectors.toList())));
    }

    public List<PolicyRole> getRoles(ServiceType serviceType, String requestUrl, HttpMethod httpMethod) {
        return cachedRoles.getUnchecked(new ComposedKey(serviceType, requestUrl, httpMethod));
    }

    private List<PolicyRole> loadRoles(ServiceType serviceType, String requestUrl, HttpMethod httpMethod) {
        String path = PolicyHelper.extractPath(requestUrl);

        return servicePoliciesMap.computeIfAbsent(serviceType, k -> new HashMap<>(0))
                .computeIfAbsent(httpMethod, k -> new ArrayList<>())
                .stream()
                .filter(p -> pathMatcher.match(p.getPath(), path))
                .findFirst()
                .map(Policy::getRoles)
                .orElseGet(ImmutableList::of);
    }

    class ComposedKey {
        private ServiceType serviceType;
        private String url;
        private HttpMethod httpMethod;

        public ComposedKey(ServiceType serviceType, String url, HttpMethod httpMethod) {
            this.serviceType = serviceType;
            this.url = url;
            this.httpMethod = httpMethod;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            ComposedKey that = (ComposedKey) o;

            if (serviceType != that.serviceType) {
                return false;
            }

            if (url != null ? !url.equals(that.url) : that.url != null) {
                return false;
            }
            return httpMethod == that.httpMethod;
        }

        @Override
        public int hashCode() {
            int result = serviceType != null ? serviceType.hashCode() : 0;
            result = 31 * result + (url != null ? url.hashCode() : 0);
            result = 31 * result + (httpMethod != null ? httpMethod.hashCode() : 0);
            return result;
        }
    }
}
