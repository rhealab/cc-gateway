package cc.gateway.policy;

import cc.gateway.policy.domain.Policy;
import cc.gateway.policy.domain.ServiceType;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author xzy on 17-10-16.
 */
@Component
@Path("policy/serviceType/{serviceType}")
@Produces(MediaType.APPLICATION_JSON)
public class PolicyEndpoint {

    private final PolicyService management;

    @Inject
    public PolicyEndpoint(PolicyService management) {
        this.management = management;
    }

    @GET
    public Response get(@PathParam("serviceType") ServiceType serviceType) {
        return Response.ok(management.getPolicies(serviceType)).build();
    }

    @POST
    public Response create(@PathParam("serviceType") ServiceType serviceType,
                           @Valid Policy policy) {
        policy.setServiceType(serviceType);
        management.createPolicy(serviceType, policy);
        return Response.ok("{}").build();
    }

    @PUT
    @Path("id/{id}")
    public Response update(@PathParam("serviceType") ServiceType serviceType,
                           @PathParam("id") long id,
                           @Valid Policy policy) {
        policy.setServiceType(serviceType);
        policy.setId(id);
        management.updatePolicy(policy);
        return Response.ok("{}").build();
    }

    @DELETE
    @Path("id/{id}")
    public Response delete(@PathParam("serviceType") ServiceType serviceType,
                           @PathParam("id") long id) {
        management.deletePolicy(id);
        return Response.ok("{}").build();
    }
}
