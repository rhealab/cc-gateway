package cc.gateway.policy;

import cc.gateway.policy.data.GatewayRolePolicy;
import cc.gateway.policy.data.GatewayRolePolicyRepository;
import cc.gateway.policy.domain.Policy;
import cc.gateway.policy.domain.ServiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static cc.gateway.Constants.GATEWAY_POLICY_REFRESH_NOTIFICATION;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/3/16.
 */
@Component
public class PolicyService {

    private static final Logger logger = LoggerFactory.getLogger(PolicyService.class);

    private ExecutorService executorService = Executors.newCachedThreadPool();

    @PostConstruct
    public void refresh() {
        Runnable listener = () -> {
            try {
                factory.getConnection()
                        .pSubscribe((message, pattern) -> {
                                    logger.info("received refresh policy notification!");
                                    policyManager.refreshServicePolicies();
                                },
                                GATEWAY_POLICY_REFRESH_NOTIFICATION.getBytes());
            } catch (Exception e) {
                try {
                    /*
                    sleep 5 seconds in case error storm
                     */
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    // ignore
                }
                refresh();
            }
        };

        executorService.submit(listener);
    }

    private final PolicyManager policyManager;

    private final GatewayRolePolicyRepository repository;

    private final JedisConnectionFactory factory;

    @Inject
    public PolicyService(PolicyManager policyManager,
                         GatewayRolePolicyRepository repository,
                         JedisConnectionFactory factory) {
        this.policyManager = policyManager;
        this.repository = repository;
        this.factory = factory;
    }

    public List<Policy> getPolicies(ServiceType serviceType) {
        return policyManager.getServicePolicies().get(serviceType);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updatePolicy(Policy newPolicy) {
        GatewayRolePolicy rolePolicy = Policy.to(newPolicy);
        repository.save(rolePolicy);
        policyManager.refreshServicePolicies();
        repository.flush();
        factory.getConnection().publish(GATEWAY_POLICY_REFRESH_NOTIFICATION.getBytes(), "".getBytes());
    }

    @Transactional(rollbackFor = Exception.class)
    public void updatePolicies(ServiceType serviceType, List<Policy> newPolicies) {
        Set<String> paths = newPolicies
                .stream()
                .map(Policy::getPath)
                .collect(Collectors.toSet());

        List<Policy> filteredPolicies = policyManager.getServicePolicies()
                .get(serviceType)
                .stream()
                .filter(p -> !paths.contains(p.getPath()))
                .collect(Collectors.toList());
        filteredPolicies.addAll(newPolicies);

        policyManager.getServicePolicies().put(serviceType, filteredPolicies);

        List<GatewayRolePolicy> gatewayRolePolicies = Policy.to(newPolicies);
        repository.save(gatewayRolePolicies);
        repository.flush();
        factory.getConnection().publish(GATEWAY_POLICY_REFRESH_NOTIFICATION.getBytes(), "".getBytes());
    }

    @Transactional(rollbackFor = Exception.class)
    public void createPolicy(ServiceType serviceType, Policy newPolicy) {
        GatewayRolePolicy rolePolicy = Policy.to(newPolicy);
        repository.save(rolePolicy);
        repository.flush();

        policyManager.getServicePolicies()
                .get(serviceType)
                .add(newPolicy);
        factory.getConnection().publish(GATEWAY_POLICY_REFRESH_NOTIFICATION.getBytes(), "".getBytes());
    }

    @Transactional(rollbackFor = Exception.class)
    public void createPolicies(ServiceType serviceType, List<Policy> newPolicies) {
        List<GatewayRolePolicy> gatewayRolePolicies = Policy.to(newPolicies);
        repository.save(gatewayRolePolicies);
        repository.flush();
        policyManager.getServicePolicies()
                .get(serviceType)
                .addAll(newPolicies);
        factory.getConnection().publish(GATEWAY_POLICY_REFRESH_NOTIFICATION.getBytes(), "".getBytes());
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletePolicy(long id) {
        repository.delete(id);
        repository.flush();
        policyManager.refreshServicePolicies();
        factory.getConnection().publish(GATEWAY_POLICY_REFRESH_NOTIFICATION.getBytes(), "".getBytes());
    }
}
