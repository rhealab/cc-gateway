package cc.gateway.policy;

import cc.keystone.client.domain.RoleType;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/3/16.
 */
public enum PolicyRole {
    ANONYMOUS(null),
    FORBIDDEN(null),
    SYS_ADMIN(RoleType.SYS_ADMIN),
    NS_ADMIN(RoleType.NS_ADMIN),
    DEVELOPER(RoleType.DEVELOPER);

    private RoleType keystoneRoleType;

    PolicyRole(RoleType keystoneRoleType) {
        this.keystoneRoleType = keystoneRoleType;
    }

    public RoleType getKeystoneRoleType() {
        return keystoneRoleType;
    }
}
