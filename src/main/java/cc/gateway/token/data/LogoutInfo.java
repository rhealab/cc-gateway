package cc.gateway.token.data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/10/30.
 */

@Entity
@Table(
        indexes = @Index(name = "logout_info_i1", columnList = "keystoneToken")
)
public class LogoutInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String keystoneToken;

    private Date expirationTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKeystoneToken() {
        return keystoneToken;
    }

    public void setKeystoneToken(String keystoneToken) {
        this.keystoneToken = keystoneToken;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    @Override
    public String toString() {
        return "LogoutInfo{" +
                "id='" + id + '\'' +
                ", keystoneToken='" + keystoneToken + '\'' +
                ", expirationTime=" + expirationTime +
                '}';
    }
}
