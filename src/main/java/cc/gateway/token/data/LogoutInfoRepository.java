package cc.gateway.token.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/10/30.
 */
public interface LogoutInfoRepository extends JpaRepository<LogoutInfo, String> {

    /**
     * find the logout info by keystone token
     * @param keystoneToken the keystone token
     * @return logout info
     */
    LogoutInfo findLogoutInfoByKeystoneToken(String keystoneToken);
}
