package cc.gateway.token;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/3/20.
 */
public class TokenHelper {
    public static String extractToken(ContainerRequestContext req) {
        return extractToken(req.getHeaderString("Authorization"));
    }

    public static String extractToken(HttpServletRequest req) {
        return extractToken(req.getHeader("Authorization"));
    }

    public static String extractToken(String header) {
        if (header == null) {
            return null;
        }
        return header.length() > "Bearer ".length() ? header.substring("Bearer ".length()) : null;
    }
}
