package cc.gateway.token;

import cc.gateway.token.data.LogoutInfo;
import cc.gateway.token.data.LogoutInfoRepository;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.security.interfaces.RSAPrivateKey;
import java.text.ParseException;

/**
 * @author wangchunyang@gmail.com
 */
@Component
public class TokenManager {
    private static final Logger logger = LoggerFactory.getLogger(TokenManager.class);
    public static final String USERNAME = "u";
    public static final String USER_ID = "uid";
    public static final String WRAPPED_TOKEN = "wtk";

    public static final String SOURCE = "src";
    public static final String RECEIVE = "recv";

    private final RSAKeyPairReader rsaKeyPairReader;
    private final LogoutInfoRepository logoutInfoRepository;

    private JWSVerifier verifier;
    private JWSVerifier userVerifier;

    @Inject
    public TokenManager(RSAKeyPairReader rsaKeyPairReader, LogoutInfoRepository logoutInfoRepository) {
        this.rsaKeyPairReader = rsaKeyPairReader;
        this.logoutInfoRepository = logoutInfoRepository;
    }

    @PostConstruct
    public void init() {
        verifier = createVerifier();
        userVerifier = createUserVerifier();
    }

    private JWSVerifier createVerifier() {
        return createRSAVerifier();
    }

    private JWSVerifier createUserVerifier() {
        return createUserRSAVerifier();
    }

    private JWSVerifier createRSAVerifier() {
        return new RSASSAVerifier(rsaKeyPairReader.readPublicKey("keys/public_key.der"));
    }

    private JWSVerifier createUserRSAVerifier() {
        return new RSASSAVerifier(rsaKeyPairReader.readPublicKey("keys/public.der"));
    }

    public Token decodeToken(String jwtToken) {
        if (jwtToken == null) {
            return null;
        }
        Token token = new Token();
        try {
            SignedJWT signedJWT = SignedJWT.parse(jwtToken);
            boolean verified = signedJWT.verify(verifier);
            token.setValid(verified);
            if (verified) {
                JWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();
                token.setUsername((String) claimsSet.getClaim(USERNAME));
                token.setUserId((String) claimsSet.getClaim(USER_ID));
                token.setWrappedToken((String) claimsSet.getClaim(WRAPPED_TOKEN));
                token.setExpirationTime(claimsSet.getExpirationTime());

                // TODO read these deleted token from redis may has better performance
                if (token.getExpirationTime().getTime() > System.currentTimeMillis()) {
                    LogoutInfo deletedInfo = logoutInfoRepository.findLogoutInfoByKeystoneToken(token.getWrappedToken());
                    if (deletedInfo != null) {
                        token.setValid(false);
                    }
                } else {
                    token.setValid(false);
                }
            }
        } catch (ParseException | JOSEException e) {
            logger.error("Failed to verify token: {}", jwtToken, e);
        }
        return token;
    }

    public String encodeSecurityToken(SecurityToken token) {
        RSAPrivateKey privateKey = rsaKeyPairReader.readPrivateKey("keys/private.der");
        JWSSigner signer = new RSASSASigner(privateKey);

        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .claim(SOURCE, token.getSource())
                .claim(USER_ID, token.getUserId())
                .claim(RECEIVE, token.getReceiver())
                .expirationTime(token.getExpirationTime())
                .build();

        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.RS256), claimsSet);
        try {
            signedJWT.sign(signer);
            return signedJWT.serialize();
        } catch (JOSEException e) {
            logger.error("failed to encrypt : {}", e);
            return null;
        }
    }

    public SecurityToken decodeUserToken(String token) {
        if (token == null) {
            return null;
        }
        try {
            SignedJWT signedJWT = SignedJWT.parse(token);
            boolean verified = signedJWT.verify(userVerifier);
            SecurityToken securityToken = new SecurityToken();
            securityToken.setValid(verified);
            if (verified) {
                JWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();
                String userId = (String) claimsSet.getClaim(USER_ID);
                String source = (String) claimsSet.getClaim(SOURCE);
                String receive = (String) claimsSet.getClaim(RECEIVE);
                securityToken.setUserId(userId);
                securityToken.setReceiver(receive);
                securityToken.setSource(source);
                securityToken.setExpirationTime(claimsSet.getExpirationTime());
                return securityToken;

            }
        } catch (ParseException | JOSEException e) {
            logger.error("Failed to verify token: {}", token, e);
        }
        return null;
    }
}
