package cc.gateway;

/**
 * @author wangchunyang@gmail.com
 */
public interface Constants {
    String REQ_ID = "x-gw-req-id";
    String USER_ID = "x-gw-user-id";

    String CLIENT_TYPE = "x-client-type";
    String CLIENT_TYPE_CONSOLE = "console";
    String CLIENT_TYPE_ENNCTL = "ennctl";
    String CLIENT_TYPE_OTHERS = "others";

    String DEFAULT_CLUSTER_NAME = "sh";

    String GW_STOPWATCH = "x-gw-stopwatch";
    String SERVICE_STOPWATCH = "x-service-stopwatch";
    String KEYSTONE_ELAPSED_GET_ROLE_ASSIGNMENTS = "x-keystone-elapsed-get-role-assignments";
    String SECURITY_ID = "x-gw-security-id";

    String API_SERVER_NAME = "apiserver";
    String CC_BACKEND_SERVER_NAME = "backend";
    String CC_NOTIFICATION_SERVER_NAME = "notification";
    String CC_KUBEORIGIN_SERVER_NAME = "origin";
    String CC_HARBOR_SERVER_NAME = "harbor";
    String CC_TEMPLATE_SERVER_NAME = "template";
    String CC_INITIALIZER_SERVER_NAME = "initializer";
    String CC_ACCOUNT_SERVER_NAME = "account";
    String CC_AUDIT_SERVER_NAME = "audit";
    String CC_LOG_SERVER_NAME = "log";
    String CC_METRIC_SERVER_NAME = "metric";
    String CC_WATCH_SERVER_NAME = "watch";
    String CC_STORAGE_SERVER_NAME = "storage";
    String DF_CORE_SVC_NAME = "df_core_svc";
    String DF_METRIC_SVC_NAME = "df_metric_svc";
    String CC_GC_SERVER_NAME = "gc";
    String CC_TENSORFLOW_SERVER_NAME = "tensorflow";
    String DF_MONITOR_SVC_NAME = "df_monitor_svc";

    /**
     * redis
     */
    String REDIS_PRIVILEGE_GROUP_KEY = "cc.privilege.group";
    String REDIS_SVC_PRIVILEGE_KEY = "cc.svc.privilege";
    String REDIS_PRIVILEGE_LIST_KEY = "cc.privilege.list";
    String REDIS_SVC_PRIVILEGE_LIST_KEY = "cc.svc.list";

    String GATEWAY_POLICY_REFRESH_NOTIFICATION = "cc.gateway.policy.refresh";

    String TOKEN_DELETED_NOTIFICATION = "cc.token.delete";

    String GROUP_ANONYMOUS = "ANONYMOUS";

    String IGNORE_FILTERS = "ignore.filters";
}
