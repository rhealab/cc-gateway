package cc.gateway.error;

import cc.lib.retcode.CcException;
import com.netflix.zuul.context.RequestContext;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;

import java.util.HashMap;
import java.util.Map;

import static cc.gateway.Constants.*;

/**
 * @author wangchunyang@gmail.com
 */
@Component
public class AppErrorAttributes extends DefaultErrorAttributes {
    private Map<String, Err> errMapper = new HashMap<>();

    {
        errMapper.put(API_SERVER_NAME, Err.API_SERVER_ERROR);
        errMapper.put(CC_BACKEND_SERVER_NAME, Err.CC_BACKEND_SERVER_ERROR);
        errMapper.put(CC_NOTIFICATION_SERVER_NAME, Err.CC_NOTIFICATION_SERVER_ERROR);
        errMapper.put(CC_KUBEORIGIN_SERVER_NAME, Err.CC_KUBEORIGIN_SERVER_ERROR);
        errMapper.put(CC_HARBOR_SERVER_NAME, Err.CC_HARBOR_SERVER_ERROR);
        errMapper.put(CC_ACCOUNT_SERVER_NAME, Err.CC_ACCOUNT_SERVER_ERROR);
        errMapper.put(CC_AUDIT_SERVER_NAME, Err.CC_AUDIT_SERVER_ERROR);
        errMapper.put(CC_LOG_SERVER_NAME, Err.CC_LOG_SERVER_ERROR);
        errMapper.put(CC_METRIC_SERVER_NAME, Err.CC_METRIC_SERVER_ERROR);
        errMapper.put(CC_WATCH_SERVER_NAME, Err.CC_WATCH_SERVER_ERROR);
    }

    @Override
    public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
        Err err = Err.SERVER_ERROR;
        Object serverNameObj = RequestContext.getCurrentContext().get(FilterConstants.PROXY_KEY);
        if (serverNameObj != null) {
            err = errMapper.getOrDefault(serverNameObj.toString(), err);
        }

        Map<String, Object> payload = new HashMap<>(1);
        Throwable throwable = getError(requestAttributes);
        if (throwable != null) {
            if (throwable instanceof CcException) {
                payload.put("root_cause", ((CcException) throwable).getPayload());
            } else if (throwable.getCause() != null) {
                payload.put("root_cause", throwable.getCause().getMessage());
            }
        }

        return ErrHelper.composeAttribute(err, payload);
    }
}
