package cc.gateway.error;

import org.springframework.core.NestedRuntimeException;

/**
 * @author wangchunyang@gmail.com
 */
public class AppException extends NestedRuntimeException {
    public AppException(String msg) {
        super(msg);
    }

    public AppException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
