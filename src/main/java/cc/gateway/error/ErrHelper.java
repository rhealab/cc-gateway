package cc.gateway.error;

import com.google.common.collect.ImmutableMap;

import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/3/19.
 */
public class ErrHelper {
    public static Map<String, Object> composeAttribute(Err err, Map<String, Object> payload) {
        return ImmutableMap.<String, Object>builder()
                .put("code", err.getCode())
                .put("message", err.getMessage())
                .put("payload", payload)
                .build();
    }

    public static Response composeResponse(Err err) {
        ImmutableMap<String, Object> entity = ImmutableMap.<String, Object>builder()
                .put("code", err.getCode())
                .put("message", err.getMessage())
                .put("payload", ImmutableMap.of("detail", err.getMessage()))
                .build();
        return Response.status(err.getHttpStatus()).entity(entity).build();
    }
}
