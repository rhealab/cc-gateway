package cc.gateway.error;

/**
 * @author wangchunyang@gmail.com
 */
public enum Err {
    USER_NOT_AUTHENTICATED(8090001, "the user's token is invalid", 401),
    USER_NOT_AUTHORIZED(8090002, "the user has no permission to access the resources", 403),
    CC_BACKEND_SERVER_ERROR(8090003, "the console backend server is not reachable currently", 500),
    CC_NOTIFICATION_SERVER_ERROR(8090004, "the console notification server is not reachable currently", 500),
    CC_KUBEORIGIN_SERVER_ERROR(8090005, "the console kubeorigin server is not reachable currently", 500),
    API_SERVER_ERROR(8090006, "the kubernetes api server is not reachable currently", 500),
    CC_HARBOR_SERVER_ERROR(8090007, "the console harbor server is not reachable currently", 500),
    CC_ACCOUNT_SERVER_ERROR(8090008, "the console account server is not reachable currently", 500),
    CC_AUDIT_SERVER_ERROR(8090009, "the console audit server is not reachable currently", 500),
    CC_LOG_SERVER_ERROR(8090010, "the console log server is not reachable currently", 500),
    CC_METRIC_SERVER_ERROR(8090012, "the console metric server is not reachable currently", 500),
    CC_WATCH_SERVER_ERROR(8090013, "the console watch server is not reachable currently", 500),
    SERVER_ERROR(4, "Internal server error", 500),

    POLICY_ALREADY_EXISTS(8090033, "the policy already exists!", 409),
    POLICY_NOT_EXISTS(8090034, "the policy not exists!", 404),

    KEYSTONE_SERVER_ERROR(601, "the keystone server is not reachable", 500),
    KEYSTONE_ADMIN_TOKEN_CACHE_INVALID(603, "the keystone admin token cache is not valid", 500),
    KEYSTONE_ADMIN_TOKEN_LOAD_FAILED(604, "failed to load the keystone admin token", 500),
    KEYSTONE_ROLE_ASSIGNMENT_LOAD_FAILED(609, "load role assignment from keystone not success", 500),

    RENDER_FREEMARKER_TEMPLATE_ERROR(8000069, "render freemarker template error", 500);

    private final int code;
    private final String message;
    private final int httpStatus;

    Err(int code, String message, int httpStatus) {
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String toString() {
        return "Err{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", httpStatus=" + httpStatus +
                '}';
    }
}
