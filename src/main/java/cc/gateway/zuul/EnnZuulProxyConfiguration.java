/*
 * Copyright 2013-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.gateway.zuul;

import org.springframework.cloud.netflix.zuul.ZuulProxyConfiguration;
import org.springframework.cloud.netflix.zuul.filters.post.SendResponseFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * EnnSendResponseFilter add a method writeLogFollowResponse for log -f
 *
 * @author normanwang06@gmail.com (wangjinxin) on 2018/1/24.
 */
@Configuration
public class EnnZuulProxyConfiguration extends ZuulProxyConfiguration {

    @Override
    @Bean
    public SendResponseFilter sendResponseFilter() {
        return new EnnSendResponseFilter();
    }
}
