package cc.gateway.filters.pre;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.inject.Inject;

/**
 * @author mye(Xizhe Ye) on 17-11-4.
 */
public class InternalPreFilter extends ZuulFilter {
    
    @Inject
    InternalPreFilterExecutor internalPreFilterExecutor;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 5;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        String serviceName = RequestContext.getCurrentContext().get(FilterConstants.PROXY_KEY).toString();
        internalPreFilterExecutor.execute(FilterHelper.getServiceTypeByStr(serviceName));

        return null;
    }
}
