package cc.gateway.filters.pre;

import cc.gateway.error.Err;
import cc.gateway.policy.PermissionManager;
import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.policy.domain.ServiceType;
import cc.gateway.token.Token;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import java.util.Optional;

import static cc.gateway.Constants.CC_TEMPLATE_SERVER_NAME;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/8/18.
 */
public class TemplatePreFilter extends ZuulFilter {
    @Inject
    private PreFilterExecutor preFilterExecutor;

    @Inject
    PermissionManager permissionManager;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10;
    }

    @Override
    public boolean shouldFilter() {
        return CC_TEMPLATE_SERVER_NAME.equals(RequestContext.getCurrentContext().get(FilterConstants.PROXY_KEY));
    }

    @Override
    public Object run() {
//        HttpServletRequest req = RequestContext.getCurrentContext().getRequest();
//        String url = req.getRequestURL().toString();
//        HttpMethod httpMethod = HttpMethod.valueOf(req.getMethod());
//        Token token = preFilterExecutor.extractToken();
//
//        if (!permissionManager.isUserAllowed(ServiceType.TEMPLATE, url,
//            httpMethod,
//            Optional.ofNullable(token).orElse(new Token()).getUserId())) {
//            FilterHelper.setErrorResponse(Err.USER_NOT_AUTHORIZED);
//            return null;
//        }
        return preFilterExecutor.execute(ServiceType.TEMPLATE);
    }

}
