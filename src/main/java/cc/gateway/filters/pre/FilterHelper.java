package cc.gateway.filters.pre;

import cc.gateway.error.Err;
import cc.gateway.error.ErrResponse;
import cc.gateway.policy.domain.ServiceType;
import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import com.netflix.zuul.context.RequestContext;

/**
 * @author wangchunyang@gmail.com
 */
public class FilterHelper {
    public static void setErrorResponse(Err err) {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.setSendZuulResponse(false);
        ctx.getResponse().setHeader("Content-Type", "application/json");
        ctx.setResponseBody(new Gson().toJson(ErrResponse.build(err)));
        ctx.setResponseStatusCode(err.getHttpStatus());
    }

    public static void startStopwatch(String name) {
        RequestContext.getCurrentContext().set(name, Stopwatch.createStarted());
    }

    public static void setResponseCharacterEncoding() {
        RequestContext.getCurrentContext().getResponse().setCharacterEncoding("UTF-8");
    }

    public static void setRequestHeader(String key, String value) {
        // If the request headers previously contained a header for the key,
        // the old value is replaced by the specified one.
        RequestContext.getCurrentContext().getZuulRequestHeaders().put(key, value);
    }

    public static String getRequestHeader(String key) {
        return RequestContext.getCurrentContext().getZuulRequestHeaders().get(key);
    }

    public static ServiceType getServiceTypeByStr(String serviceName) {
        if (serviceName.equals("apiserver")) {
            return ServiceType.API_SERVER;
        } else {
            return ServiceType.valueOf(serviceName.toUpperCase());
        }
    }
}
