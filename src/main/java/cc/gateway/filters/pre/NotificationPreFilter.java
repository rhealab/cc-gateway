package cc.gateway.filters.pre;

import cc.gateway.policy.domain.ServiceType;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.inject.Inject;

import static cc.gateway.Constants.CC_NOTIFICATION_SERVER_NAME;

/**
 * @author Created by mrzihan.tang@gmail.com on 07/06/2017.
 */
public class NotificationPreFilter extends ZuulFilter {
    @Inject
    private PreFilterExecutor preFilterExecutor;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10;
    }

    @Override
    public boolean shouldFilter() {
        return CC_NOTIFICATION_SERVER_NAME.equals(RequestContext.getCurrentContext().get(FilterConstants.PROXY_KEY));
    }

    @Override
    public Object run() {
        return preFilterExecutor.execute(ServiceType.NOTIFICATION);
    }

}
