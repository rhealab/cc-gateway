package cc.gateway.filters.pre;

import cc.gateway.policy.domain.ServiceType;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.inject.Inject;

import static cc.gateway.Constants.CC_STORAGE_SERVER_NAME;

/**
 * @author yzx12 on 17-12-5.
 */
public class StoragePreFilter extends ZuulFilter {
    @Inject
    private PreFilterExecutor preFilterExecutor;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10;
    }

    @Override
    public boolean shouldFilter() {
        return CC_STORAGE_SERVER_NAME.equals(RequestContext.getCurrentContext().get(FilterConstants.PROXY_KEY));
    }

    @Override
    public Object run() {
        return preFilterExecutor.execute(ServiceType.STORAGE);
    }
}
