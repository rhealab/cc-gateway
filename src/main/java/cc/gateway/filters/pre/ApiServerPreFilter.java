package cc.gateway.filters.pre;

import cc.gateway.Constants;
import cc.gateway.GatewayProperties;
import cc.gateway.error.AppException;
import cc.gateway.error.Err;
import cc.gateway.policy.PermissionManager;
import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.policy.domain.ServiceType;
import cc.keystone.client.KeystoneClient;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Map;
import java.util.UUID;

import static cc.gateway.Constants.API_SERVER_NAME;
import static cc.gateway.Constants.DEFAULT_CLUSTER_NAME;
import static cc.gateway.filters.pre.FilterHelper.setRequestHeader;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;

/**
 * @author wangchunyang@gmail.com
 */
public class ApiServerPreFilter extends ZuulFilter {
    private static final Logger logger = LoggerFactory.getLogger(ApiServerPreFilter.class);

    @Inject
    private PermissionManager permissionManager;

    @Inject
    private KeystoneClient keystoneClient;

    @Inject
    private GatewayProperties properties;

    private String authHeader;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public boolean isStaticFilter() {
        return false;
    }

    @Override
    public int filterOrder() {
        return PRE_DECORATION_FILTER_ORDER + 10;
    }

    @Override
    public boolean shouldFilter() {
        return API_SERVER_NAME.equals(RequestContext.getCurrentContext().get(FilterConstants.PROXY_KEY));
    }

    @PostConstruct
    public void init() {
        authHeader = "Basic " + Base64.getEncoder().encodeToString((properties.getApiServerAdminUsername() + ":" + properties.getApiServerAdminPassword()).getBytes());
    }

    @Override
    public Object run() {
        setRequestHeader(Constants.REQ_ID, UUID.randomUUID().toString());

        String clientType = RequestContext.getCurrentContext().getRequest().getHeader(Constants.CLIENT_TYPE);
        if (clientType == null) {
            clientType = Constants.CLIENT_TYPE_OTHERS;
        }
        setRequestHeader(Constants.CLIENT_TYPE, clientType);

        FilterHelper.startStopwatch(Constants.GW_STOPWATCH);

        if (checkPermission()) {
            FilterHelper.setRequestHeader("authorization", authHeader);
        }
        return null;
    }

    public boolean checkPermission() {
        if (!properties.isAuthEnabled()) {
            return true;
        }

        HttpServletRequest req = RequestContext.getCurrentContext().getRequest();
        String url = req.getRequestURL().toString();
        HttpMethod httpMethod = HttpMethod.valueOf(req.getMethod());
        if (permissionManager.isAnonymousAllowed(ServiceType.API_SERVER, url, httpMethod)) {
            return true;
        }

        if (permissionManager.isForbidden(ServiceType.API_SERVER, url, httpMethod)) {
            FilterHelper.setErrorResponse(Err.USER_NOT_AUTHORIZED);
            return false;
        }

        Map<String, String> map = extractUsernamePassword(req);
        return !map.isEmpty() && isUserAllowed(map.get("u"), map.get("p"), url, httpMethod);
    }


    public Map<String, String> extractUsernamePassword(HttpServletRequest req) {
        String authHeader = req.getHeader("authorization");
        if (authHeader != null && authHeader.startsWith("Basic ")) {
            String content = new String(Base64.getDecoder().decode(authHeader.substring("Basic ".length())));
            if (content.contains(":")) {
                String[] values = content.split(":");
                String username = values[0];
                String password = values[1];
                if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
                    setRequestHeader(Constants.USER_ID, username);
                    return ImmutableMap.of("u", username, "p", password);
                }
            }
        }
        logger.debug("Requires valid basic authorization header");
        return ImmutableMap.of();
    }

    private boolean isUserAllowed(String username, String password, String url, HttpMethod httpMethod) {
        try {
            keystoneClient.login(DEFAULT_CLUSTER_NAME, username, password);
        } catch (CcException e) {
            logger.info("The password is not correct for username '{}'", username);
            FilterHelper.setErrorResponse(Err.USER_NOT_AUTHENTICATED);
            return false;
        }

        try {
            boolean allowed = permissionManager.isUserAllowed(ServiceType.API_SERVER, url, httpMethod, username);

            if (allowed) {
                return true;
            }

            logger.info("The user is not allowed to access the url. username={}, url={}, httpMethod={}",
                    username, url, httpMethod.toString());
            FilterHelper.setErrorResponse(Err.USER_NOT_AUTHORIZED);
        } catch (AppException e) {
            return false;
        }
        return false;
    }
}
