package cc.gateway.filters.pre;

import cc.gateway.policy.domain.ServiceType;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.inject.Inject;

import static cc.gateway.Constants.DF_CORE_SVC_NAME;

/**
 * @author mye(Xizhe Ye) on 17-12-21.
 */
public class DfCorePreFilter extends ZuulFilter {
    @Inject
    private PreFilterExecutor preFilterExecutor;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10;
    }

    @Override
    public boolean shouldFilter() {
        return DF_CORE_SVC_NAME.equals(RequestContext.getCurrentContext().get(FilterConstants.PROXY_KEY));
    }

    @Override
    public Object run() {
        return preFilterExecutor.execute(ServiceType.DF_CORE_SVC);
    }
}
