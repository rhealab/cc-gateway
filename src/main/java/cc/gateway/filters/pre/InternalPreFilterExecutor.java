package cc.gateway.filters.pre;

import cc.gateway.Constants;
import cc.gateway.GatewayProperties;
import cc.gateway.error.AppException;
import cc.gateway.policy.PermissionManager;
import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.policy.domain.ServiceType;
import cc.gateway.security.SecurityMgr;
import cc.gateway.token.SecurityToken;
import cc.gateway.token.Token;
import cc.gateway.token.TokenHelper;
import cc.gateway.token.TokenManager;
import com.google.common.collect.ImmutableMap;
import com.netflix.zuul.context.RequestContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static cc.gateway.filters.pre.FilterHelper.*;

/**
 * @author mye(Xizhe Ye) on 17-11-4.
 */
@Component
public class InternalPreFilterExecutor {
    private static final Logger logger = LoggerFactory.getLogger(InternalPreFilter.class);

    @Inject
    private SecurityMgr securityMgr;

    @Inject
    TokenManager tokenManager;

    @Inject
    PermissionManager permissionManager;

    @Value("${security.allowAll}")
    boolean allowAll;

    @Inject
    private GatewayProperties properties;

    public Object execute(ServiceType service) {
        setRequestHeader(Constants.REQ_ID, UUID.randomUUID().toString());

        String clientType = RequestContext.getCurrentContext().getRequest().getHeader(Constants.CLIENT_TYPE);
        if (clientType == null) {
            clientType = Constants.CLIENT_TYPE_OTHERS;
        }
        setRequestHeader(Constants.CLIENT_TYPE, clientType);

        setResponseCharacterEncoding();
        startStopwatch(Constants.GW_STOPWATCH);

        if (!properties.isAuthEnabled()) {
            addJWTToken2Header(service.name(), new Token().getUserId());
            return null;
        }

        String securityHeader = RequestContext.getCurrentContext().getRequest().getHeader(Constants.SECURITY_ID);
        // if Security header is not null, then this is from console
        // it may not contain user-id
        // check if request is from kubectl
        if (securityHeader == null) {
            HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
            String authHeader = request.getHeader("authorization");
            Map<String, String> basicAuthMap = extractUsernamePassword(request);
            if ((authHeader == null
                || !authHeader.startsWith("Bearer "))
                && basicAuthMap.isEmpty()) {
                HttpServletRequest req = RequestContext.getCurrentContext().getRequest();
                String url = req.getRequestURL().toString();
                HttpMethod httpMethod = HttpMethod.valueOf(req.getMethod());
                if (!permissionManager.isAnonymousAllowed(service, url, httpMethod)) {
                    logger.info("auth header and security header are all null, and not anonymous allowed error");
                    return null;
                }
            }

            String tokenStr = authHeader != null &&
                authHeader.length() >= "Bearer ".length() &&
                authHeader.startsWith("Bearer ") ?
                    authHeader.substring("Bearer ".length()) : null;
            Token token = tokenManager.decodeToken(tokenStr);
            if (basicAuthMap.isEmpty()) {
                addJWTToken2Header(service.name(), Optional.ofNullable(token).orElse(new Token()).getUserId());
            } else {
                addJWTToken2Header(service.name(), basicAuthMap.getOrDefault("p", ""));
            }
        } else {
            String rawToken = TokenHelper.extractToken(securityHeader);
            SecurityToken securityToken = tokenManager.decodeUserToken(rawToken);

            if (securityToken != null && checkInternalPermission(service, securityToken.getUserId())) {
                startStopwatch(Constants.SERVICE_STOPWATCH);

                buildNewSecurityHeader(service, securityToken);
                RequestContext.getCurrentContext().getZuulRequestHeaders().put(Constants.IGNORE_FILTERS, "TRUE");
                return true;
            }
        }
        return null;
    }


    public Map<String, String> extractUsernamePassword(HttpServletRequest req) {
        String authHeader = req.getHeader("authorization");
        if (authHeader != null && authHeader.startsWith("Basic ")) {
            String content = new String(Base64.getDecoder().decode(authHeader.substring("Basic ".length())));
            if (content.contains(":")) {
                String[] values = content.split(":");
                String username = values[0];
                String password = values[1];
                if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
                    setRequestHeader(Constants.USER_ID, username);
                    return ImmutableMap.of("u", username, "p", password);
                }
            }
        }
        logger.debug("Requires valid basic authorization header");
        return ImmutableMap.of();
    }

    private void buildNewSecurityHeader(ServiceType serviceType, SecurityToken securityToken) {
        securityToken.setReceiver(serviceType.name());
        String rawToken = tokenManager.encodeSecurityToken(securityToken);
        setRequestHeader(Constants.SECURITY_ID, "Bearer " + rawToken);
    }

    public boolean checkInternalPermission(ServiceType serviceType, String userId) {
        if (allowAll) {
            return true;
        }

        HttpServletRequest req = RequestContext.getCurrentContext().getRequest();
        String url = req.getRequestURL().toString();
        HttpMethod httpMethod = HttpMethod.valueOf(req.getMethod());
        if (securityMgr.isAnonymousAllowed(serviceType.name(), url, httpMethod)) {
            logger.info("userId : {} is anonymously allowed", userId);
            return true;
        }
        return securityMgr.checkSvcAllowByUserId(userId, serviceType.name(), url, httpMethod);
    }

    private void addJWTToken2Header(String receiver, String userId) {
        SecurityToken token = new SecurityToken();
        token.setSource("GW");
        token.setUserId(userId);
        token.setReceiver(receiver);
        Date dt = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.add(Calendar.DATE, 30);
        token.setExpirationTime(dt);

        String tokenStr = tokenManager.encodeSecurityToken(token);
        if (tokenStr == null) {
            throw new AppException("token is null");
        }

        tokenStr = "Bearer " + tokenStr;
        setRequestHeader(Constants.SECURITY_ID, tokenStr);
    }
}
