package cc.gateway.filters.pre;

import cc.gateway.Constants;
import cc.gateway.GatewayProperties;
import cc.gateway.error.AppException;
import cc.gateway.error.Err;
import cc.gateway.policy.PermissionManager;
import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.policy.domain.ServiceType;
import cc.gateway.token.SecurityToken;
import cc.gateway.token.Token;
import cc.gateway.token.TokenHelper;
import cc.gateway.token.TokenManager;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

import static cc.gateway.filters.pre.FilterHelper.*;

/**
 * @author wangchunyang@gmail.com
 */
@Component
public class PreFilterExecutor {
    private static final Logger logger = LoggerFactory.getLogger(PreFilterExecutor.class);
    private final PermissionManager permissionManager;

    private final TokenManager tokenManager;

    private final GatewayProperties properties;

    @Inject
    public PreFilterExecutor(PermissionManager permissionManager,
                             TokenManager tokenManager,
                             GatewayProperties properties) {
        this.permissionManager = permissionManager;
        this.tokenManager = tokenManager;
        this.properties = properties;
    }

    public Object execute(ServiceType service) {
        setRequestHeader(Constants.REQ_ID, UUID.randomUUID().toString());

        String clientType = RequestContext.getCurrentContext().getRequest().getHeader(Constants.CLIENT_TYPE);
        if (clientType == null) {
            clientType = Constants.CLIENT_TYPE_OTHERS;
        }
        setRequestHeader(Constants.CLIENT_TYPE, clientType);

        setResponseCharacterEncoding();
        startStopwatch(Constants.GW_STOPWATCH);

        // TODO : fix me to remove redundant code
        String securityHeader = RequestContext.getCurrentContext().getRequest().getHeader(Constants.SECURITY_ID);

        String rawToken = TokenHelper.extractToken(securityHeader);
        SecurityToken token = tokenManager.decodeUserToken(rawToken);
        if (token != null && token.isValid()) {
            String userId = token.getUserId();
            logger.debug("The userId was found in token: {}", userId);

            setRequestHeader(Constants.USER_ID, userId);
        }

        String flag = RequestContext.getCurrentContext().getZuulRequestHeaders().get(Constants.IGNORE_FILTERS);
        // if ignore filter
        if (flag != null && Boolean.valueOf(flag)) {
            return null;
        }
        if (checkPermission(service)) {
            startStopwatch(Constants.SERVICE_STOPWATCH);
        }
        return null;
    }

    private boolean checkPermission(ServiceType service) {
        if (!properties.isAuthEnabled()) {
            return true;
        }

        HttpServletRequest req = RequestContext.getCurrentContext().getRequest();
        String url = req.getRequestURL().toString();
        HttpMethod httpMethod = HttpMethod.valueOf(req.getMethod());
        if (permissionManager.isAnonymousAllowed(service, url, httpMethod)) {
            Token token = extractToken();
            String userId;
            if (token != null && token.isValid()) {
                userId = token.getUserId();
                setRequestHeader(Constants.USER_ID, userId);
                logger.debug("The user is Anonymously allowed: {}", userId);
            }
            return true;
        }

        Token token = extractToken();
        String userId;
        if (token != null && token.isValid()) {
            userId = token.getUserId();
            setRequestHeader(Constants.USER_ID, userId);
            logger.debug("The userId was found in token: {}", userId);
        } else {
            logger.info("The token is empty or invalid");
            setErrorResponse(Err.USER_NOT_AUTHENTICATED);
            return false;
        }
        return isUserAllowed(service, userId, url, httpMethod);
    }

    private boolean isUserAllowed(ServiceType service, String userId, String url, HttpMethod httpMethod) {
        try {
            if (permissionManager.isUserAllowed(service, url, httpMethod, userId)) {
                return true;
            }
            logger.info("The user is not allowed to access url. userId={}, url={}, httpMethod={}",
                    userId, url, httpMethod.toString());
            setErrorResponse(Err.USER_NOT_AUTHORIZED);
        } catch (AppException e) {
            return false;
        }
        return false;
    }

    public Token extractToken() {
        HttpServletRequest req = RequestContext.getCurrentContext().getRequest();
        return tokenManager.decodeToken(TokenHelper.extractToken(req));
    }
}
