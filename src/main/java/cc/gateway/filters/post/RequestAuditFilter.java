package cc.gateway.filters.post;

import cc.gateway.GatewayProperties;
import cc.gateway.KafkaProperties;
import cc.gateway.error.AppException;
import cc.gateway.messaging.RabbitConfig;
import cc.gateway.policy.NamespaceExtractor;
import cc.lib.mq.DfKafkaProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.util.HTTPRequestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cc.gateway.Constants.*;

/**
 * @author wangchunyang@gmail.com
 */
public class RequestAuditFilter extends ZuulFilter {
    private static final Logger logger = LoggerFactory.getLogger(RequestAuditFilter.class);

    @Inject
    private GatewayProperties props;

    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    @Inject
    private KafkaProperties kafkaProperties;

    private DfKafkaProducer kafkaProducer;

    @Override
    public String filterType() {
        return FilterConstants.POST_TYPE;
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        if (kafkaProperties.isEnable()) {
            sendKafka();
        } else {
            audit();
        }
        return null;
    }

    @PostConstruct
    private void init() {
        if (kafkaProperties.isEnable()) {
            String url = kafkaProperties.getBootstrapServers()
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
            String request_audit = kafkaProperties.getTopics().get("request-audit");
            kafkaProducer = new DfKafkaProducer.Builder()
                .setUrl(url)
                .setTopic(request_audit)
                .build();
        }
    }

    private void sendKafka() {
        RequestContext ctx = RequestContext.getCurrentContext();
        String proxyKey = (String) ctx.get(FilterConstants.PROXY_KEY);
        String url = ctx.getRequest().getRequestURL().toString();

        RequestAuditMessage message = new RequestAuditMessage.Builder()
            .extra(proxyKey + "_elapsed", elapsed((Stopwatch) ctx.get(SERVICE_STOPWATCH)))
            .extra("keystone_elapsed_get_role_assignments", ctx.get(KEYSTONE_ELAPSED_GET_ROLE_ASSIGNMENTS))
            .elapsed(elapsed((Stopwatch) ctx.get(GW_STOPWATCH)))
            .service(RequestAuditMessage.ServiceType.GATEWAY)
            .requestId(ctx.getZuulRequestHeaders().get(REQ_ID))
            .userId(ctx.getZuulRequestHeaders().get(USER_ID))
            .clientType(ctx.getZuulRequestHeaders().get(CLIENT_TYPE))
            .url(url)
            .namespaceName(NamespaceExtractor.extract(url))
            .httpMethod(ctx.getRequest().getMethod())
            .httpStatus(ctx.getResponse().getStatus())
            .clientIp(HTTPRequestUtils.getInstance().getClientIP(ctx.getRequest()))
            .build();

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String msgString = objectMapper.writeValueAsString(message);
            logger.info("[topic=console-audit] RequestAuditMessage={}", message);
            if (props.isAuditEnabled()) {
                kafkaProducer.send("request_audit", msgString);
            }
        } catch (JsonProcessingException e) {
            throw new AppException("Failed to deserialize json", e);
        }
    }

    private void audit() {
        RequestContext ctx = RequestContext.getCurrentContext();
        String proxyKey = (String) ctx.get(FilterConstants.PROXY_KEY);
        String url = ctx.getRequest().getRequestURL().toString();

        RequestAuditMessage message = new RequestAuditMessage.Builder()
                .extra(proxyKey + "_elapsed", elapsed((Stopwatch) ctx.get(SERVICE_STOPWATCH)))
                .extra("keystone_elapsed_get_role_assignments", ctx.get(KEYSTONE_ELAPSED_GET_ROLE_ASSIGNMENTS))
                .elapsed(elapsed((Stopwatch) ctx.get(GW_STOPWATCH)))
                .service(RequestAuditMessage.ServiceType.GATEWAY)
                .requestId(ctx.getZuulRequestHeaders().get(REQ_ID))
                .userId(ctx.getZuulRequestHeaders().get(USER_ID))
                .clientType(ctx.getZuulRequestHeaders().get(CLIENT_TYPE))
                .url(url)
                .namespaceName(NamespaceExtractor.extract(url))
                .httpMethod(ctx.getRequest().getMethod())
                .httpStatus(ctx.getResponse().getStatus())
                .clientIp(HTTPRequestUtils.getInstance().getClientIP(ctx.getRequest()))
                .build();


        CompletableFuture.runAsync(() -> {
            logger.info("[topic=console-audit] RequestAuditMessage={}", message);
            if (props.isAuditEnabled()) {
                messagingTemplate.convertAndSend(RabbitConfig.REQUEST_AUDIT_Q, message);
            }
        });
    }

    private Long elapsed(Stopwatch stopwatch) {
        if (stopwatch == null) {
            return null;
        }
        return stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
    }
}
