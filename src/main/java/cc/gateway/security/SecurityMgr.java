package cc.gateway.security;

import cc.gateway.Constants;
import cc.gateway.error.AppException;
import cc.gateway.policy.domain.HttpMethod;
import cc.gateway.security.data.PrivilegeGroup;
import cc.gateway.security.data.PrivilegeGroupWrapper;
import cc.gateway.security.data.SvcPrivilege;
import cc.gateway.security.data.SvcPrivilegeWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import redis.clients.jedis.exceptions.JedisConnectionException;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;


/**
 * @author mye(Xizhe Ye) on 17-11-7.
 */
@Component
public class SecurityMgr {
    private static final Logger logger = LoggerFactory.getLogger(SecurityMgr.class);
    @Autowired
    private RedisTemplate<String, PrivilegeGroupWrapper> privilegeGroupRedisTemplate;

    @Autowired
    private RedisTemplate<String, SvcPrivilegeWrapper> svcPrivilegeRedisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private ValueOperations<String, PrivilegeGroupWrapper> privilegeWrapperOps;

    private ValueOperations<String, SvcPrivilegeWrapper> svcOps;

    private ValueOperations<String, String> stringOps;

    private PathMatcher matcher = new AntPathMatcher();


    @PostConstruct
    private void initOps() {
        privilegeWrapperOps = privilegeGroupRedisTemplate.opsForValue();
        svcOps = svcPrivilegeRedisTemplate.opsForValue();
        stringOps = stringRedisTemplate.opsForValue();
    }

    public List<String> getPrivilegeGroups() {
        String listStr = stringOps.get(Constants.REDIS_PRIVILEGE_LIST_KEY);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return listStr != null ? mapper.readValue(listStr, List.class) : Lists.newArrayList();
        } catch (IOException e) {
            return null;
        }
    }

    public void addPrivilegeGroup(String groupType) {
        List<String> lists = getPrivilegeGroups();
        if (lists == null) {
            lists = new ArrayList<>();
        }
        lists.add(groupType);
        ObjectMapper mapper = new ObjectMapper();
        try {
            String toWrite = mapper.writeValueAsString(lists);
            stringOps.set(Constants.REDIS_PRIVILEGE_LIST_KEY, toWrite);
        } catch (JsonProcessingException e) {
            logger.error("json parse error");
        }
    }

    public void addUser2PrivilegeGroup(String userId, String groupType, boolean forced) throws AppException {
        if (userId == null || groupType == null) {
            return;
        }

        try {
            String key = getPrivilegeGroupKey(groupType);
            privilegeWrapperOps.setIfAbsent(key, new PrivilegeGroupWrapper.Builder()
                    .setGroupType(groupType).setGroups(Lists.newArrayList()).build());
            PrivilegeGroupWrapper groupWrapper = getGroupWrapper(key);
            if (groupWrapper == null) {
                throw new AppException("redis set privilege group failed");
            }

            List<PrivilegeGroup> groups = groupWrapper.getGroups();

            boolean hasUserId = groups.stream().anyMatch(group -> group.getUserId().equals(userId));
            if (!hasUserId) {
                PrivilegeGroup newGroup = new PrivilegeGroup();
                newGroup.setUserId(userId);
                newGroup.setGroupName(groupType);
                groups.add(newGroup);
            } else if (forced) {
                for (ListIterator<PrivilegeGroup> it = groups.listIterator(); it.hasNext(); ) {
                    PrivilegeGroup group = it.next();
                    if (group.getUserId().equals(userId)) {
                        group.setUserId(userId);
                        group.setGroupName(groupType);
                        it.set(group);
                    }
                }
            }
            groupWrapper.setGroups(groups);
            privilegeWrapperOps.set(key, groupWrapper);
        } catch (JedisConnectionException e) {
            logger.error("cannot connect to redis : " + e);
        }

    }

    public List<PrivilegeGroup> getUserIdsByGroup(String groupName) {
        String key = getPrivilegeGroupKey(groupName);
        PrivilegeGroupWrapper wrapper = getGroupWrapper(key);
        if (wrapper == null) {
            logger.debug("wrapper is null, no such group name");
            throw new AppException("");
        }
        return wrapper.getGroups();
    }

    public PrivilegeGroupWrapper getGroupWrapper(String key) {
        return privilegeWrapperOps.get(key);
    }


    private List<String> getGroupTypeByUserId(String userId) {
        if (userId == null) {
            throw new AppException("userId is null");
        }

        try {
            List<String> retList = Lists.newArrayList();
            List<String> groupList = getPrivilegeGroups();
            groupList.forEach(groupName -> {
                List<PrivilegeGroup> privilegeGroups = getUserIdsByGroup(groupName);

                boolean hasMatched = privilegeGroups.stream().anyMatch(group -> group.getUserId().equals(userId));
                if (hasMatched) {
                    retList.add(groupName);
                }
            });

            return retList;
        } catch (JedisConnectionException e) {
            logger.error("cannot connect to redis : " + e);
            return null;
        }
    }


    public List<String> getSvcPrivilegeList() {
        String listStr = stringOps.get(Constants.REDIS_SVC_PRIVILEGE_LIST_KEY);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return listStr != null ? mapper.readValue(listStr, List.class) : Lists.newArrayList();
        } catch (IOException e) {
            return null;
        }
    }

    public void addSvcPrivilege2List(String serviceType) {
        List<String> lists = getSvcPrivilegeList();
        if (lists == null) {
            lists = new ArrayList<>();
        }
        lists.add(serviceType);
        ObjectMapper mapper = new ObjectMapper();
        try {
            String toWrite = mapper.writeValueAsString(lists);
            stringOps.set(Constants.REDIS_SVC_PRIVILEGE_LIST_KEY, toWrite);
        } catch (JsonProcessingException e) {
            logger.error("json parse error");
        }
    }


    private SvcPrivilegeWrapper getSvcPrivilegeWrapper(String key) {
        return svcOps.get(key);
    }

    public List<SvcPrivilege> getSvcPrivileges(String serviceType) {
        String key = getSvcPrivilegeKey(serviceType);
        SvcPrivilegeWrapper wrapper = svcOps.get(key);
        return wrapper != null ? wrapper.getPrivileges() : null;
    }

    public void storeSvcPrivilege(String serviceType, SvcPrivilege svcPrivilege, boolean forced) {
        if (svcPrivilege == null || svcPrivilege.getServiceName() == null) {
            return;
        }

        try {
            String key = getSvcPrivilegeKey(serviceType);
            svcOps.setIfAbsent(key, new SvcPrivilegeWrapper.Builder().setServiceType(serviceType)
                    .setPrivileges(Lists.newArrayList()).build());
            SvcPrivilegeWrapper wrapper = getSvcPrivilegeWrapper(key);
            List<SvcPrivilege> privileges = wrapper.getPrivileges();
            boolean hasSvc = privileges.stream()
                    .anyMatch(privilege -> hasSvcPrivilege(privilege,
                            svcPrivilege.getServiceName(),
                            svcPrivilege.getUrl(),
                            svcPrivilege.getMethodType()));
            if (!hasSvc) {
                privileges.add(svcPrivilege);
            } else if (forced) {
                for (ListIterator<SvcPrivilege> it = privileges.listIterator(); it.hasNext(); ) {
                    SvcPrivilege tmp = it.next();
                    if (hasSvcPrivilege(tmp, svcPrivilege.getServiceName(), svcPrivilege.getUrl(), svcPrivilege.getMethodType())) {
                        it.set(svcPrivilege);
                    }
                }
            }
            svcOps.set(key, wrapper);
        } catch (JedisConnectionException e) {
            throw new AppException("cannot set to redis");
        }
    }

    private boolean hasSvcPrivilege(SvcPrivilege svcPrivilege, String serviceType, String path, HttpMethod method) {
        return svcPrivilege != null &&
                svcPrivilege.getMethodType() != null &&
                svcPrivilege.getUrl() != null &&
                svcPrivilege.getServiceName() != null &&
                serviceType != null &&
                path != null &&
                method != null &&
                svcPrivilege.getServiceName().equals(serviceType) &&
                svcPrivilege.getUrl().equals(path) &&
                svcPrivilege.getMethodType().equals(method);
    }

    private List<SvcPrivilege> getSvcPrivilegeByType(String serviceType, String path, HttpMethod method) {
        SvcPrivilegeWrapper wrapper = getSvcPrivilegeWrapper(getSvcPrivilegeKey(serviceType));
        if (wrapper == null) {
            throw new AppException("no such svcPrivilege");
        }
        return wrapper.getPrivileges().stream()
                .filter(svcPrivilege -> svcPrivilege != null && svcPrivilege.getUrl() != null)
                .filter(svcPrivilege -> pathMatch(svcPrivilege.getUrl(), path)
                        && svcPrivilege.getMethodType().equals(method)).collect(Collectors.toList());
    }

    public boolean checkSvcAllowByUserId(String userId, String serviceType, String requestUrl, HttpMethod method) {
        if (userId == null) {
            logger.error("user id is null");
            return false;
        }
        List<String> groupTypes = getGroupTypeByUserId(userId);
        try {
            URL url = new URL(requestUrl);
            String path = url.getPath();
            List<SvcPrivilege> svcPrivileges = getSvcPrivilegeByType(serviceType, path, method);
            return svcPrivileges
                    .stream()
                    .anyMatch(svcPrivilege -> groupTypes.contains(svcPrivilege.getUserGroup()));

        } catch (MalformedURLException e) {
            logger.error("the URL is invalid");
            return false;
        }
    }

    public boolean isAnonymousAllowed(String serviceType, String requestUrl, HttpMethod method) {
        try {
            URL url = new URL(requestUrl);
            String path = url.getPath();
            List<SvcPrivilege> svcPrivileges = getSvcPrivilegeByType(serviceType, path, method);
            return svcPrivileges.stream().anyMatch(svcPrivilege -> svcPrivilege.getUserGroup().equals(Constants.GROUP_ANONYMOUS));
        } catch (Exception e) {
            return false;
        }
    }

    private boolean pathMatch(String dbUrl, String path) {
        return matcher.match(dbUrl, path);
    }


    private String getPrivilegeGroupKey(String group) {
        return Constants.REDIS_PRIVILEGE_GROUP_KEY + ":" + group;
    }

    private String getSvcPrivilegeKey(String serviceType) {
        return Constants.REDIS_SVC_PRIVILEGE_KEY + ":" + serviceType;
    }
}
