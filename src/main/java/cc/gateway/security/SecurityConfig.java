package cc.gateway.security;

import cc.gateway.security.data.PrivilegeGroupWrapper;
import cc.gateway.security.data.SvcPrivilegeWrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author mye(Xizhe Ye) on 17-11-8.
 */
@Configuration
public class SecurityConfig {

    @Bean
    public RedisTemplate<String, PrivilegeGroupWrapper> redisGroupTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, PrivilegeGroupWrapper> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        return template;
    }

    @Bean
    public RedisTemplate<String, SvcPrivilegeWrapper> redisSvcTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, SvcPrivilegeWrapper> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        return template;
    }

    @Bean
    public StringRedisTemplate stringTemplate(RedisConnectionFactory connectionFactory) {
        StringRedisTemplate template = new StringRedisTemplate();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        return template;
    }
}
