package cc.gateway.security.data;

import java.io.Serializable;
import java.util.List;

/**
 * @author mye(Xizhe Ye) on 17-11-9.
 */
public class SvcPrivilegeWrapper implements Serializable {
    private String serviceType;
    private List<SvcPrivilege> privileges;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public List<SvcPrivilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<SvcPrivilege> privileges) {
        this.privileges = privileges;
    }


    public static class Builder {
        String serviceType;
        List<SvcPrivilege> privileges;

        public Builder setServiceType(String serviceType) {
            this.serviceType = serviceType;
            return this;
        }

        public Builder setPrivileges(List<SvcPrivilege> privileges) {
            this.privileges = privileges;
            return this;
        }

        public SvcPrivilegeWrapper build() {
            SvcPrivilegeWrapper wrapper = new SvcPrivilegeWrapper();
            wrapper.setServiceType(serviceType);
            wrapper.setPrivileges(privileges);
            return wrapper;
        }
    }
}
