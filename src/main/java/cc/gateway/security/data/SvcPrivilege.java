package cc.gateway.security.data;

import cc.gateway.policy.domain.HttpMethod;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author mye(Xizhe Ye) on 17-11-3.
 */
@Entity
public class SvcPrivilege implements Serializable {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String serviceName;

    @Enumerated
    @Column(nullable = false)
    private HttpMethod methodType;

    @Column(nullable = false)
    private String url;

    @Column(nullable = false)
    private String userGroup;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public HttpMethod getMethodType() {
        return methodType;
    }

    public void setMethodType(HttpMethod methodType) {
        this.methodType = methodType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }
}
