package cc.gateway.security.data;

import java.io.Serializable;
import java.util.List;

/**
 * @author mye(Xizhe Ye) on 17-11-9.
 */
public class PrivilegeGroupWrapper implements Serializable {
    private String groupType;
    private List<PrivilegeGroup> groups;
    private static final long serialVersionUID = -130464747494596154L;

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public List<PrivilegeGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<PrivilegeGroup> groups) {
        this.groups = groups;
    }

    public static class Builder {
        private String groupType;
        List<PrivilegeGroup> groups;

        public String getGroupType() {
            return groupType;
        }

        public Builder setGroupType(String groupType) {
            this.groupType = groupType;
            return this;
        }

        public List<PrivilegeGroup> getGroups() {
            return groups;
        }

        public Builder setGroups(List<PrivilegeGroup> groups) {
            this.groups = groups;
            return this;
        }

        public PrivilegeGroupWrapper build() {
            PrivilegeGroupWrapper group = new PrivilegeGroupWrapper();
            group.setGroupType(groupType);
            group.setGroups(groups);
            return group;
        }
    }
}
